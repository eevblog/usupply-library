#pragma once
#include "USB.hpp"
#include "StackPacket.hpp"
#include "StaticLambdaWrapper.hpp"
#include "RunOnConstruct.hpp"
#include "RunOnDestruct.hpp"

namespace USB
{
    using USBAction = Peripherals::USBAction;
    #define PRODUCT_NAME "uSupply - "
    constexpr Version HIDVersion{ 1, 11 };
    //
    //
    static constexpr U<1>   IN_Id   = 1;
    static constexpr U<1>   OUT_Id  = 2;
    using                   HIDPacket = HID::Packet<30>;
    /**
     * @brief The usupply hid interface class.
     * 
     * @tparam F The type of the callback for data received.
     */
    template <typename F>
    struct HIDInterface
    {
        using ReceiveCallback_t = General::StaticLambdaWrapper<F>;
        ReceiveCallback_t m_ReceiveCallback;
        /**
         * @brief Construct a new HIDInterface object
         * 
         * @tparam C The type of the callback.
         */
        template <typename C>
        HIDInterface(C && receive_callback) noexcept:
            m_ReceiveCallback{ std::forward<C>( receive_callback ) }
        {}
        /**
         * @brief Whether or not a transaction is pending.
         * 
         * @warning If this class is accessed from multiple contexts this needs to be volatile.
         */
        inline static bool Pending = false;
        /**
         * @brief A unique ID for the string index.
         */
        inline static constexpr U<1>        StringIndex = UniqueID();
        /**
         * @brief The endpoints used by the interface.
         */
        inline static constexpr U<1>        Endpoints[] = { 1, 0 };
        /**
         * @brief The string descriptor for the HID interface.
         */
        inline static constexpr auto        String      = PRODUCT_NAME "HID";
        /**
         * @brief The interface number.
         */
        inline static constexpr unsigned    Number      = 0;
        /**
         * @brief This being empty indicates that the transmit endpoint is idle
         */
        static inline TransmitData TXData{};
        /**
         * @brief A barrier to optimisations around the TXData.
         * 
         * @warning This is volatile access instead of volatile data, while GCC 
         * does support this, they have publically stated they may not in the future.
         * A simular pattern is used in the linux kernal.
         */
        static inline TransmitData * volatile   TXDataPtr   { nullptr };
        /**
         * @brief HID Report descriptor
         */
        inline static constexpr auto HIDReportDescriptor = USB::ByteArray
        (
            0x05, 0x01,                     // USAGE_PAGE (Generic Desktop)
            0x09, 0x00,                     // USAGE (Undefined)
            0xa1, 0x01,                     // COLLECTION (Application)
            0x15, 0x00,                     //   LOGICAL_MINIMUM (0)
            0x26, 0xff, 0x00,               //   LOGICAL_MAXIMUM (255)
            0x85, IN_Id,                    //   REPORT_ID (1)
            0x75, 0x08,                     //   REPORT_SIZE (8)
            0x95, HIDPacket::Length(),      //   REPORT_COUNT
            0x09, 0x00,                     //   USAGE (Undefined)
            0x81, 0x82,                     //   INPUT (Data,Var,Abs,Vol) - to the host
            0x85, OUT_Id,                   //   REPORT_ID (2)
            0x75, 0x08,                     //   REPORT_SIZE (8)
            0x95, HIDPacket::Length(),      //   REPORT_COUNT
            0x09, 0x00,                     //   USAGE (Undefined)
            0x91, 0x82,                     //   OUTPUT (Data,Var,Abs,Vol) - from the host
            0xc0                            // END_COLLECTION
        );
        /**
         * @brief The HID class descriptor
         */
        inline static constexpr HID::Descriptor ClassDescriptor
        {
            HIDVersion,
            HID::CountryCodes::US,
            PackedTuple
            {
                HID::DescriptorAllocation
                {
                    DescriptorTypes::HID_Report,
                    sizeof(HIDReportDescriptor)
                }
            }
        };
        /**
         * @brief The endpoint descriptor 0.
         */
        inline static constexpr StandardEndpointDescriptor EndpointA
        {
            {},
            EndpointAddress{ Endpoints[0], Direction::IN },
            { EndpointAttributes::Interrupt },
            sizeof(HIDPacket),       // Max packet size
            1u                      // 1 ms
        };
        /**
         * @brief The interface descriptor.
         */
        inline static constexpr HID::AutoInterface Descriptor
        {
            Number,
            HID::SubClassCodes::None,
            HID::ProtocolCodes::None,
            StringIndex,
            ClassDescriptor,
            EndpointA
        };
        /**
         * @brief Transmits an empty IN packet.
         */
        static ALWAYS_INLINE void TransmitEmpty() noexcept
        {
            USB_LL::template ActionBound<typename EP1::EP, Peripherals::USBGeneral::Direction::IN>( { USBAction::TransmitEmpty } );
        }
        /**
         * @brief HID Specific requests
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::GetDescriptor value ) noexcept
        {
            switch ( value.Data.Type )
            {
            case DescriptorTypes::HID_Report:
            {
                action( { USBAction::TransmitRaw, RBytes( HIDReportDescriptor, value.wLength ) } );
                TXDataPtr = &TXData;
                /**
                 * @brief Kickstart the empty packet process, this prevents host from putting device to sleep.
                 */
                TransmitEmpty();
                break;
            }
            case DescriptorTypes::HID:
            case DescriptorTypes::HID_PhysicalDescriptor:
            {
                action( { USBAction::Stall } );
                break;
            }
            default:
            {
                break;
            }
            }
        }
        /**
         * @brief Called when a GetStatus descriptor is recieved.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Get Status descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, GetStatus value ) noexcept
        {
            static constexpr InterfaceStatus desc;
            action( { USBAction::TransmitRaw, RBytes( desc, value.wLength ) } );
        }
        /**
         * @brief Called when SetReport descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Set Report descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::SetReport value ) noexcept
        {
            action( { USBAction::ReceiveRaw } );
        }
        /**
         * @brief Called when Set Idle descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Set Idle descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::SetIdle value ) noexcept
        {
            action( { USBAction::Status } );
        }
        /**
         * @brief Called when Set Descriptor descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Set Descriptor descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::SetDescriptor value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Get Report descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Get Report descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::GetReportBasic value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Get Report descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Get Report descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::GetReportSpecific value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Get Idle descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Get Idle descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::GetIdle value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Get Protocol descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Get Protocol descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::GetProtocol value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Set Protocol descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Set Protocol descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, HID::SetProtocol value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Set Interface descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Set Interface descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, SetInterface value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Called when Get Interface descriptor is received.
         * 
         * @param endpoint The endpoint number.
         * @param action Allows correct access to hardware.
         * @param value The received Get Interface descriptor. 
         */
        static ALWAYS_INLINE void Request( uint8_t endpoint, USBActionCallback action, GetInterface value ) noexcept
        {
            action( { USBAction::Stall } );
        }
        /**
         * @brief Data has been recieved by the USB peripheral
         * 
         * This function triggers an event.
         * 
         * @param endpoint The endpoint that recieved data.
         * @param action The callback that allows safe actions on the USB peripheral.
         * @param value The recieved data.
         */
        static ALWAYS_INLINE bool RecieveComplete( uint8_t endpoint, USBActionCallback action, Containers::DataView<uint8_t> const & value ) noexcept
        {
            if ( endpoint == 0 )
            {
                /**
                 * This might be strange but the USB peripheral NAK's transactions while the peripheral is 
                 * occupied. For this reason much of the grunt work is done here. This is a form of 
                 * flow control.
                 */
                ReceiveCallback_t::Run( endpoint, value );
                /**
                 * The data is complete.
                 */
                action( { USBAction::Status } );
                return true;
            }
            else
            {
                return false;
            }
        }
        /**
         * @brief Runs when a status transaction is completed.
         * 
         * This occurs from receive or transmit completion
         * 
         * @param endpoint The endpoint number which triggered the complete.
         * @param action The callback that allows safe actions on the USB peripheral.
         * @return true Returns true when the the function handled the completion.
         * @return false when the completion was not handled by this interface.
         */
        static ALWAYS_INLINE bool StatusComplete( uint8_t endpoint, USBActionCallback action ) noexcept
        {
            if ( endpoint == 0 )
            {
                action( { USBAction::EndStatus } );
                return true;
            }
            else
            {
                return false;
            }
        }
        /**
         * @brief Runs at an intermediate state of a transaction.
         * 
         * This occurs only from transmits. It triggers a further transmit if more data is avaliable.
         * 
         * @param endpoint The endpoint that the transmit is on.
         * @param action The callback that allows safe actions on the USB peripheral.
         */
        static void TransmitContinue( USBActionCallback action ) noexcept
        {
            if ( HIDPacket packet{ IN_Id }; packet.Assign( TXDataPtr->Consume( HIDPacket::Capacity() ) ) )
            {
                action( { USBAction::TransmitRaw, RBytes( packet ) } );
            }
        }
        /**
         * @brief Schedules a keep alive packet
         * 
         * This is just an empty junk packet which convinces the host the
         * device is still being used. This is what is needed to prevent 
         * the host requesting the device suspend.
         */
        static bool KeepAlive( USBActionCallback action ) noexcept
        {
            action( { USBAction::TransmitEmpty } );
            return true;
        }
        /**
         * @brief The callback that is run when a transmission is complete.
         * 
         * @param endpoint The endpoint number.
         * @param action The action class which allows interaction with underlying hardware.
         * @return true The transmit complete was handled.
         * @return false The transmit complete was unhandled.
         */
        static bool TransmitComplete( uint8_t endpoint, USBActionCallback action ) noexcept
        {
            switch ( endpoint )
            {
                case 0:
                {
                    action( { USBAction::Status } );
                    return true;
                }
                case 1:
                {
                    if ( TXDataPtr and TXDataPtr->PopFront( HIDPacket::Capacity() ).Size() ) 
                    {
                        TransmitContinue( action );
                    }
                    else
                    {
                        KeepAlive( action );
                    }
                    return true;
                }
                default:
                {
                    return false;
                }
            }
        }
        /**
         * @brief Transmits data on the HID interface
         * 
         * This triggers a transmit on the IN endpoint for the HID interface
         * 
         * @param endpoint the endpoint to transfer with (not used)
         * @param data the data to transfer
         * @return true When transfer is intiated successfully.
         * @return false When transfer is unable to intiate, likely because the endpoint is busy.
         */
        static bool Transmit( uint8_t endpoint, TransmitData const & data ) noexcept
        {
            if ( TXDataPtr and TXDataPtr->Empty() and data.Size() )
            {
                *TXDataPtr = data;
                TransmitContinue( USB_LL::template ActionBound<typename EP1::EP, Peripherals::USBGeneral::Direction::IN> );
                return true;
            }
            //
            return false;
        }
        /**
         * @brief A functor wrapper than stores a reference to an endpoint action callback.
         */
        struct InterfaceCallback
        {
            USBActionCallback const & action;
            //
            template <typename T>
            void operator()( uint8_t endpoint, T && packet ) const noexcept
            {
                HIDInterface::Request( endpoint, action, std::forward<T>( packet ) );
            }
        };
        /**
         * @brief Takes a standard request and routes it to a specific request
         * 
         * @param endpoint The endpoint that the request was sent to.
         * @param action A function which allows access to safe endpoint specific actions.
         * @param request The request recieved by the endpoint.
         * @return ALWAYS_INLINE Transform 
         */
        static ALWAYS_INLINE bool Transform( uint8_t endpoint, USBActionCallback action, StandardDeviceRequest request ) noexcept
        {
            return HID::Transform
            (
                endpoint,
                request,
                InterfaceCallback{ action }
            );
        }
    };
    /**
     * @brief The guide for the template arguments of HIDInterface
     * 
     * @tparam F The callback function type.
     */
    template <typename F>
    HIDInterface(F) -> HIDInterface<F>;
}