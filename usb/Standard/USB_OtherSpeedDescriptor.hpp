#pragma once
#include "USB_Types.hpp"

namespace USB
{
    #pragma pack(push, 1)
    /**
     * @brief A other speed descriptor.
     * 
     * @note Refer to USB 2.0 specifications.
     */
    struct OtherSpeedDescriptor : StandardHeader<OtherSpeedDescriptor, DescriptorTypes::OTHER_SPEED_CONFIGURATION>
    {
        U<2> wTotalLength;
        U<1> bNumInterfaces;
        U<1> bConfigurationValue;
        U<1> iConfiguration;
        U<1> bmAttributes;
        MaxPower bMaxPower;
    };
    #pragma pack(pop)
}