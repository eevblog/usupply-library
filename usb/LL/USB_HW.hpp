#pragma once
#include "PinCommon.hpp"
#include "RCC.hpp"
#include "Power.hpp"
#include "RegistersUSB.hpp"
#include "Interrupt.hpp"
#include "FIFO.hpp"
#include "DataView.hpp"
#include "SysTick.hpp"
#include "RegistersRCC.hpp"
#include "RegistersSysCfg.hpp"
#include "Macros.hpp"
#include "USB_Types.hpp"
#include "RAII.hpp"
/**
 * 
 */
namespace Peripherals
{
	/**
	 * 
	 */
	using namespace IO;
	using namespace RCCGeneral;
	using Direction = USBGeneral::Direction;
	/**
	 * Types used for transmit and Receive
	 */
	using TransmitData_t 	= USB::TransmitData;
	using ReceiveData_t 	= USB::ReceiveData;
	/**
	 * Defines a general purpose function that allows information to be transfered from the platform agnositc layer to the platform specific layer.
	 */
	enum class USBTransferType : uint32_t
	{
		IN,
		OUT,
		SETUP,
		UNKNOWN
	};
	enum class EndpointDirection : uint16_t
	{
		None,
		IN,
		OUT,
		IN_OUT
	};
    enum class EndpointType : uint16_t
    {
        Bulk 			= 0b00,
        Control 		= 0b01,
        Isochronous 	= 0b10,
        Interrupt 		= 0b11
    };
	enum class BulkKind : uint16_t
	{
		SingleBuffer 	= 0,
		DoubleBuffered 	= 1
	};
	enum class ControlKind : uint16_t
	{
		Normal			= 0,
		StatusOut		= 1
	};
	enum class Status : uint32_t
	{
		Disabled 	= 0b00u,
		Stall 		= 0b01u,
		NAK 		= 0b10u,
		Valid 		= 0b11u
	};
	enum class Address : std::uint8_t
	{
		Undefined = 0xFF
	};
	enum class USBEvent
	{
		EndpointSetup,
		WakeupEvent,
		SuspendEvent,
		TransmitComplete,
		StatusComplete,
		StartOfFrame
	};
	enum class USBWakeupEvent : uint16_t
	{
		RootReset	= 0b00,
		NoiseOnBus	= 0b10, 
		RootResume	= 0b01,
		Unknown		= 0b11
	};
	enum class USBReceiveType : uint16_t
	{
		OUT = 0,
		SETUP = 1,
	};
	enum class ModeBCD : uint32_t
	{
		SDP,
		CDP,
		DCP,
		ProprietryOrPS2
	};
	enum class USBAction : std::uint32_t
	{
		Nothing,
		ClearToReceive,
		Status,
		ClearStatusFlag,
		EndStatus,
		Stall,
		SetAddress,
		ReceiveNAK,
		ReceiveStall,
		ReceiveValid,
		ReceiveRaw,
		TransmitNAK,
		TransmitStall,
		TransmitValid,
		TransmitRaw,
		Enable,
		Disable,
		Connect,
		Disconnect,
		TriggerStall,
		TriggerStatusStage,
		TransmitEmpty,
		TransmitComplete,
		UseOtherEndpoint
	};
	/**
	 * The USBModule states for initalisation
	 */
	template<uint16_t number, EndpointDirection direction, uint16_t buffer_size, EndpointType endpoint, auto kind, Status inital_transmit_state = Status::Stall, Status inital_receive_state = Status::Valid>
	struct Endpoint
	{
		static constexpr uint16_t 	Number 					= number;
		static constexpr uint16_t 	BufferSize 				= buffer_size;
		static constexpr uint16_t 	Direction 				= General::UnderlyingValue(direction);
		static constexpr uint16_t 	Type 					= General::UnderlyingValue(endpoint);
		static constexpr uint16_t 	Kind 					= kind;
		static constexpr Status 	InitialTransmitState 	= inital_transmit_state;	// IN
		static constexpr Status 	InitialReceiveState 	= inital_receive_state;		// OUT Peripherals::USBGeneral::EP<0>
		//
		using EP = Peripherals::USBGeneral::EP<number>;
	};
	template <uint16_t number, EndpointDirection direction, uint16_t buffer_size, BulkKind kind, Status inital_transmit_state, Status inital_receive_state>  
	struct Endpoint<number, direction, buffer_size, EndpointType::Bulk, kind, inital_transmit_state, inital_receive_state>
	{
		static constexpr uint16_t 	Number	 				= number;
		static constexpr uint16_t 	BufferSize				= buffer_size;
		static constexpr uint16_t 	Direction 				= General::UnderlyingValue(direction);
		static constexpr uint16_t 	Type 					= General::UnderlyingValue(EndpointType::Bulk);
		static constexpr uint16_t 	Kind 					= General::UnderlyingValue(kind);
		static constexpr Status 	InitialTransmitState 	= inital_transmit_state;	// IN	
		static constexpr Status 	InitialReceiveState 	= inital_receive_state;		// OUT
		//
		using EP = Peripherals::USBGeneral::EP<number>;
	};
	template <uint16_t number, EndpointDirection direction, uint16_t buffer_size, ControlKind kind, Status inital_transmit_state, Status inital_receive_state>
	struct Endpoint<number, direction, buffer_size, EndpointType::Control, kind, inital_transmit_state, inital_receive_state>
	{
		static constexpr uint16_t 	Number 				 	= number;
		static constexpr uint16_t 	BufferSize 			 	= buffer_size;
		static constexpr uint16_t 	Direction 			 	= General::UnderlyingValue(direction);
		static constexpr uint16_t 	Type 				 	= General::UnderlyingValue(EndpointType::Control);
		static constexpr uint16_t 	Kind 				 	= General::UnderlyingValue(kind);
		static constexpr Status 	InitialTransmitState 	= inital_transmit_state;	// IN
		static constexpr Status 	InitialReceiveState 	= inital_receive_state;		// OUT
		//
		using EP = Peripherals::USBGeneral::EP<number>;
	};
	/**
	 * 
	 */
	struct USBActionData;
	/**
	 * 
	 */
	using TransmitCallback 		= TransmitData_t (*)(TransmitData_t, bool) noexcept;
	using USBActionCallback 	= void (*)(USBActionData data) noexcept;
	//
	struct OtherActionData
	{
		uint8_t Endpoint = 0;
		USBActionData * const DataPtr = nullptr;
	};
	//
	struct USBActionData
	{
		USBAction Action{ USBAction::Nothing };
		//
		//
		struct Data_t
		{
			union
			{
				std::nullopt_t 					NoOp;	//Prevents unnessary initialisation
				Address							DeviceAddress;
				TransmitData_t					Transmit;
				OtherActionData					Other;
			};
			//
			Data_t(std::nullopt_t) : NoOp{ std::nullopt } {}
			Data_t( uint8_t value ) noexcept : 
				DeviceAddress{ value }
			{}
			Data_t( uint16_t value ) noexcept : 
				DeviceAddress{ (uint8_t)value }
			{}
			Data_t( Address value ) noexcept : 
				DeviceAddress{ value }
			{}
			Data_t( TransmitData_t data ) noexcept : 
				Transmit{ data }
			{}
			Data_t( OtherActionData data ) noexcept : 
				Other{ data }
			{}
		}
		Data{ std::nullopt };
	};
	/**
	 * The USBModule power kernal
	 */
	struct USBModulePower : GeneralPowerKernal<Peripheral::ClockUSB>
	{
		static void Construct() noexcept
		{
			/**
			 * Enable the clock for the USBModule module
			 */
			GeneralPowerKernal<Peripheral::ClockUSB>::Construct();
			/**
			 * 	This configures PLL
			 */
			RCCGeneral::CFGR3{}.USBSW() = true;
			/**
			 * Setup the alternate mode for these pins
			 */
			RCCGeneral::APB2ENR{}.SYSCFGCOMPEN()   = true;
			SysCfgGeneral::CFGR1{}.PA11_PA12_RMP() = true;
			/**
			 * Wait 10us (480 ticks at 48 MHz)
			 */
			volatile int basic_delay = 4800;
			while(basic_delay--);
		}
		/**
		 * 
		 */
		static void Destruct() noexcept
		{
			/**
			 * Return mapping to default
			 */
			SysCfgGeneral::CFGR1{}.PA11_PA12_RMP() = false;
			/**
			 * Disable the clock connection to the USBModule module.
			 */
			RCCGeneral::CFGR3{}.USBSW() = false;
			/**
			 * 
			 */
			GeneralPowerKernal<Peripheral::ClockUSB>::Destruct();
		}
	};
	/**
	 * This is the non-callback based setup class for USBModule.
	 * It does not handle and interrupt handling, it simply provides utility functions.
	 */
	template <typename Kernal, typename USBModule_DN, typename USBModule_DP>
	struct USBModuleBase : 
		USBModule_DN,
		USBModule_DP,
		General::ModulePower<USBModulePower>
	{
		using USB_DP = USBModule_DP;
		using USB_DN = USBModule_DN;
		//
		struct BufferSetup
		{
			struct Details
			{
				uint16_t Size 		= 0;
				uint16_t Address 	= 0;
			};
			Details RX{};
			Details TX{};
		};
		/**
		 * This returns the endpoint specified by the index
		 * 
		 * NOTE: 	For faster access to endpoints use the templated version.
		 * 			See the implementation of this function for an example.
		 */
		ALWAYS_INLINE static Direction CurrentDirection() noexcept
		{
			return (Direction)USBGeneral::ISTR{}.DIR().Get();
		}
		ALWAYS_INLINE static uint8_t CurrentEndpoint() noexcept
		{
			return (uint8_t)USBGeneral::ISTR{}.EP_ID().Get();
		}
		/**
		 * Returns the endpoint register given the endpoint number.
		 */
		template <typename F>
		ALWAYS_INLINE static decltype(auto) GetEndpoint( uint8_t endpoint, F && function ) noexcept
		{
			using return_t = decltype( std::forward<F>( function )( USBGeneral::EP<0>{} ) );
			//
			if constexpr ( std::is_void_v<return_t> )
			{
				using namespace USBGeneral;
				switch ( endpoint )
				{
				case 0:		if constexpr (Kernal::template UseEP<0>) std::forward<F>( function )( EP<0>{} ); return;
				case 1:		if constexpr (Kernal::template UseEP<1>) std::forward<F>( function )( EP<1>{} ); return;
				case 2:		if constexpr (Kernal::template UseEP<2>) std::forward<F>( function )( EP<2>{} ); return;
				case 3:		if constexpr (Kernal::template UseEP<3>) std::forward<F>( function )( EP<3>{} ); return;
				case 4:		if constexpr (Kernal::template UseEP<4>) std::forward<F>( function )( EP<4>{} ); return;
				case 5:		if constexpr (Kernal::template UseEP<5>) std::forward<F>( function )( EP<5>{} ); return;
				case 6:		if constexpr (Kernal::template UseEP<6>) std::forward<F>( function )( EP<6>{} ); return;
				case 7:		if constexpr (Kernal::template UseEP<7>) std::forward<F>( function )( EP<7>{} ); return;
				}
			}
			else
			{
				using namespace USBGeneral;
				switch ( endpoint )
				{
				case 0: 	if constexpr (Kernal::template UseEP<0>) return std::forward<F>( function )( EP<0>{} );
				case 1: 	if constexpr (Kernal::template UseEP<1>) return std::forward<F>( function )( EP<1>{} );
				case 2: 	if constexpr (Kernal::template UseEP<2>) return std::forward<F>( function )( EP<2>{} );
				case 3:		if constexpr (Kernal::template UseEP<3>) return std::forward<F>( function )( EP<3>{} );
				case 4:		if constexpr (Kernal::template UseEP<4>) return std::forward<F>( function )( EP<4>{} );
				case 5:		if constexpr (Kernal::template UseEP<5>) return std::forward<F>( function )( EP<5>{} );
				case 6:		if constexpr (Kernal::template UseEP<6>) return std::forward<F>( function )( EP<6>{} );
				case 7:		if constexpr (Kernal::template UseEP<7>) return std::forward<F>( function )( EP<7>{} );
				}
				return return_t{};
			}
		}
		/**
		 * 
		 */
		template <typename F>
		ALWAYS_INLINE static decltype(auto) GetCurrentEndpoint( F && function ) noexcept
		{
			return GetEndpoint( CurrentEndpoint(), std::forward<F>( function ) );
		}
		/**
		 * Setup the endpoint with index "endpoint".
		 * 
		 *  This does the following:
		 *	-	Allocates a Receive buffer from the avaliable SRAM memory.
		 */
		template <typename T, size_t N>
		ALWAYS_INLINE static constexpr bool IsAccending( std::array<T, N> data ) noexcept
		{
			if (N <= 1u) return true;
			//
			for (std::size_t i = 0u; i < N - 1u; ++i)
			{
				if (data[i] >= data[i + 1u])
					return false;
			}
			return true;
		}
		/**
		 * 
		 */
		ALWAYS_INLINE static void SetAddress( Address address ) noexcept
		{
			USBGeneral::DADDR{}.Set( General::UnderlyingValue( address ) | USBGeneral::DADDR_EF_MASK );
		}
		ALWAYS_INLINE static Address GetAddress() noexcept
		{
			return (Address)(uint8_t)USBGeneral::DADDR{}.ADD().Get();
		}
		/**
		 * 
		 */
		template < typename ...Settings >
		ALWAYS_INLINE static void ConfigureEndpointsImpl( Settings ... settings ) noexcept
		{
			using namespace USBGeneral;
			static_assert( IsAccending( std::array{ Settings::Number... } ), "Endpoint addresses must be specied in accending order without duplicates." );
			/**
			 * This version of the USBModule library always places the BTABLE at the
			 * SRAM address and uses the first block of memory after that for the Receive buffers
			 * and then treats the remaining sections of the buffer like a stack for transmit operations.
			 * 
			 * This maximises the avaliable size for transmits.
			 * 
			 * NOTE : 	Concurrent multiple endpoint transmissions can collide in this setup, this
			 *  		AND the bus limit the speed of the transmission.
			 */
			uint16_t tail = PacketMemoryArea::BufferOffset;
			/**
			 * 
			 */
			( GetEndpoint
			(
				Settings::Number, 
				[&]( auto EP )
				{
					/**
					 * Create some aliases for the state variables/registers
					 */
					constexpr uint16_t 	number 	= Settings::Number;
					auto pma = GetPacketMemoryArea();
					/**
					 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					 * The EP_TYPE bits in the USBModule_EPnR register must be set according to the endpoint type, 
					 * eventually using the EP_KIND bit to enable any special required feature.
					 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					 */
					EP.EP_TYPE() = Settings::Type;
					/**
					 * Setup the endpoint address to the defined address
					 * 	This is known to be valid because GetEndpoint sanatised this scope.
					 */
					EP.EA() = number;
					/**
					 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					 * The first step to initialize an endpoint is to write appropriate values to the
					 * ADDRn_TX/ADDRn_RX registers so that the USBModule peripheral finds the data to be
					 * transmitted already available and the data to be received can be buffered. 
					 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					 *
					 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					 * This is the dedicated SRAM for the USBModule interface allocate it as per buffer size requests
					 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
					 */
					/**
					 * Initialise all endpoints as specified
					 * 	Represents the current address for the next allocation
					 */
					if constexpr (
						( Settings::Direction == General::UnderlyingValue( EndpointDirection::IN 	 ) ) ||
						( Settings::Direction == General::UnderlyingValue( EndpointDirection::IN_OUT ) ) )
					{
						/**
						 * Toggle if DTOG_RX is high.
						 * Writes of zero are ignored, this is constant time expression.
						 */
						EP.DTOG_TX() = false;
						/**
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 * Set the Receive address and the buffer size, this may not be exactly the same
						 * as the requested size. It will be the same or larger. The buffer size might be 
						 * padded to ensure 16 bit alignment of the tail address.
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 */
						pma->TransmitAllocation( EP, tail );
						tail += Settings::BufferSize;
						/**
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 * On the transmit side, 
						 * 
						 * the endpoint must be enabled using the:
						 * STAT_TX bits in the USBModule_EPnR register and COUNTn_TX must be initialized. 
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 */
						if constexpr ( Settings::InitialTransmitState == Status::Stall )
						{
							TransmitStall( EP );
						}
						else if constexpr ( Settings::InitialTransmitState == Status::NAK )
						{
							TransmitNAK( EP );
						}
						else if constexpr ( Settings::InitialTransmitState == Status::Valid )
						{
							TransmitValid( EP );
						}
					}
					/**
					 * 
					 */
					if constexpr (
						( Settings::Direction == General::UnderlyingValue( EndpointDirection::OUT 	 ) ) || 
						( Settings::Direction == General::UnderlyingValue( EndpointDirection::IN_OUT ) ) )
					{
						/**
						 * Toggle if DTOG_RX is high.
						 * Writes of zero are ignored, this is constant time expression.
						 */
						EP.DTOG_RX() = false;
						/**
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 * Set the Receive address and the buffer size, this may not be exactly the same
						 * as the requested size. It will be the same or larger.
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 */
						tail += pma->ReceiveAllocation( EP, tail, Settings::BufferSize );
						/**
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 * For reception, 
						 * 
						 * STAT_RX bits must be set to enable reception.
						 * COUNTn_RX must be written with the allocated buffer size using the 
						 * BL_SIZE and NUM_BLOCK fields.
						 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
						 */
						if constexpr (Settings::InitialReceiveState == Status::Stall)
						{
							ReceiveStall( EP );
						}
						else if constexpr (Settings::InitialReceiveState == Status::NAK)
						{
							ReceiveNAK( EP );
						}
						else if constexpr (Settings::InitialReceiveState == Status::Valid)
						{
							ReceiveValid( EP );
						}
					}
				}
			), ...);
		}
		/**
		 * Decode the wake-up event
		 */
		ALWAYS_INLINE static USBWakeupEvent DecodeWakeup() noexcept
		{
			switch ( USBGeneral::FNR{}.RX() )
			{
			case 0b01: 	return USBWakeupEvent::RootResume;
			case 0b00: 	return USBWakeupEvent::RootReset;
			case 0b11:
			case 0b10: 	return USBWakeupEvent::NoiseOnBus;
			default: 	return USBWakeupEvent::Unknown;
			}
		}
		template <typename EP_t>
		ALWAYS_INLINE static void StatusOut( EP_t EP, bool value ) noexcept
		{
			EP.EP_KIND() = value ? 1 : 0;
		}
		/**
		 * Sets the NAK flags
		 */
		template <typename EP_t>
		ALWAYS_INLINE static void TransmitNAK( EP_t EP ) noexcept
		{
			EP.STAT_TX() = General::UnderlyingValue( Status::NAK );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void ReceiveNAK( EP_t EP ) noexcept
		{
			EP.STAT_RX() = General::UnderlyingValue( Status::NAK );
		}
		/**
		 * Set the stall flags
		 */
		template <typename EP_t>
		ALWAYS_INLINE static void TransmitStall( EP_t EP ) noexcept
		{
			EP.STAT_TX() = General::UnderlyingValue( Status::Stall );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void ReceiveStall( EP_t EP ) noexcept
		{
			EP.STAT_RX() = General::UnderlyingValue( Status::Stall );
		}
		/**
		 * Set the valid flag
		 */
		template <typename EP_t>
		ALWAYS_INLINE static void ReceiveValid( EP_t EP ) noexcept
		{
			EP.STAT_RX() = General::UnderlyingValue( Status::Valid );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void TransmitValid( EP_t EP ) noexcept
		{
			EP.STAT_TX() = General::UnderlyingValue( Status::Valid );
		}
		/**
		 * Set the valid flag
		 */
		template <typename EP_t>
		ALWAYS_INLINE static bool ReceiveEnabled( EP_t EP ) noexcept
		{
			return EP.STAT_RX().Get() != General::UnderlyingValue( Status::Disabled );
		}
		template <typename EP_t>
		ALWAYS_INLINE static bool TransmitEnabled( EP_t EP ) noexcept
		{
			return EP.STAT_TX().Get() != General::UnderlyingValue( Status::Disabled );
		}
		/**
		 * Get the complete flag
		 */
		template <typename EP_t>
		ALWAYS_INLINE static bool TransmitComplete( EP_t EP ) noexcept
		{
			return EP.CTR_TX();
		}
		template <typename EP_t>
		ALWAYS_INLINE static bool ReceiveComplete( EP_t EP ) noexcept
		{
			return EP.CTR_RX();
		}
		/**
		 * Returns the type of endpoint
		 */
		template <typename EP_t>
		ALWAYS_INLINE static EndpointType Type( EP_t EP ) noexcept
		{
			return (EndpointType)EP.EP_TYPE().Get();
		}
		/**
		 * Returns a filtered transfer type
		 */
		template <typename EP_t>
		ALWAYS_INLINE static USBTransferType CurrentTransferType( EP_t EP ) noexcept
		{
			switch ( CurrentDirection() )
			{
			case Direction::IN:
				if ( TransmitComplete( EP ) )
				{
					return USBTransferType::IN;
				}
				break;
			case Direction::OUT:
				if ( ReceiveComplete( EP ) )
				{
					return (EP.SETUP() == 1) ?
						USBTransferType::SETUP:
						USBTransferType::OUT;
				}
				break;
			default:
				break;
			};
			return USBTransferType::UNKNOWN;
		}
		/**
		 * @brief Triggers a transmit on the USB peripheral
		 * 
		 * This function loads the PMA (Packet Memory Area) and then initiates a transfer.
		 * 
		 * @tparam EP_t This is the endpoint type.
		 * @param EP This is the endpoint.
		 * @param data The data to be transmitted
		 * @return ALWAYS_INLINE TransmitTriggerRaw Returns the remaining data. 
		 */
		template <typename EP_t>
		ALWAYS_INLINE static TransmitData_t TransmitTriggerRaw( EP_t EP, TransmitData_t data ) noexcept
		{
			auto output{ USBGeneral::GetPacketMemoryArea()->WriteTransmitPMA( EP, data ) };
			TransmitValid( EP );
			return output;
		}
		template <typename EP_t>
		ALWAYS_INLINE static void ClearReceiveCTR( EP_t EP ) noexcept
		{
			EP.CTR_RX() = false;
		}
		template <typename EP_t>
		ALWAYS_INLINE static void ClearTransmitCTR( EP_t EP ) noexcept
		{
			EP.CTR_TX() = false;
		}
		template <typename EP_t>
		ALWAYS_INLINE static bool IsStatusStage( EP_t EP ) noexcept
		{
			return !!EP.EP_KIND().Get();
		}
		template <typename EP_t>
		ALWAYS_INLINE static void TransmitEmpty( EP_t EP ) noexcept
		{
			USBGeneral::GetPacketMemoryArea()->TransmitLength( EP, 0 );
			TransmitValid( EP );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void TriggerStall( EP_t EP, Direction current_direction = CurrentDirection()  ) noexcept
		{
			switch ( current_direction )
			{
			case Direction::IN:
				ReceiveStall( EP );
				break;
			case Direction::OUT:
				TransmitStall( EP );
				break;
			default:
				break;
			};
		}
		/////////////////////////////////////////////////////////////////////////
		template <typename EP_t>
		ALWAYS_INLINE static void SetStatusOut( EP_t EP ) noexcept
		{
			StatusOut( EP, true );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void ClearStatusOut( EP_t EP ) noexcept
		{
			StatusOut( EP, false );
		}
		/////////////////////////////////////////////////////////////////////////
		template <typename EP_t>
		ALWAYS_INLINE static void TriggerStatusOut( EP_t EP ) noexcept
		{
			if ( ReceiveEnabled( EP ) )
			{
				SetStatusOut( EP );
				ReceiveValid( EP );
			}
		}
		template <typename EP_t>
		ALWAYS_INLINE static void TriggerStatusIn( EP_t EP ) noexcept
		{
			if ( TransmitEnabled( EP ) )
			{
				SetStatusOut( EP );
				TransmitEmpty( EP );
			}
		}
		template <typename EP_t>
		ALWAYS_INLINE static void TriggerStatusStage( EP_t EP, Direction current_direction = CurrentDirection() ) noexcept
		{
			if ( Type( EP ) == EndpointType::Control )
			{
				switch ( current_direction )
				{
				case Direction::IN:
					TriggerStatusOut( EP );
					break;
				case Direction::OUT:
					TriggerStatusIn( EP );
					break;
				default:
					break;
				};
			}
		}
		/////////////////////////////////////////////////////////////////////////
		template <typename EP_t>
		ALWAYS_INLINE static void ReceiveReady( EP_t EP ) noexcept
		{
			ReceiveValid( EP );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void ClearStatusFlag( EP_t EP ) noexcept
		{
			ClearStatusOut( EP );
		}
		template <typename EP_t>
		ALWAYS_INLINE static void EndStatusStage( EP_t EP ) noexcept
		{
			ClearStatusFlag( EP );
			ReceiveReady( EP );
		}
		ALWAYS_INLINE static void Reset() noexcept
		{
			USBGeneral::CNTR{}.FRES() = true;
			USBGeneral::CNTR{}.FRES() = false;
		}
		/**
		 * Connect / Disconnect to the host.
		 */
		ALWAYS_INLINE static void Connect() noexcept
		{
			USBGeneral::BCDR{}.DPPU() = true;
		}
		ALWAYS_INLINE static void Disconnect() noexcept
		{
			USBGeneral::BCDR{}.DPPU() = false;
		}
		/**
		 * Enable / Disables the USBModule module, this enables/disables the pullup on DP
		 */
		ALWAYS_INLINE static void Enable() noexcept
		{
			USBGeneral::DADDR{}.EF() = true;
		}
		ALWAYS_INLINE static void Disable() noexcept
		{
			USBGeneral::DADDR{}.EF() = false;
		}
		/**
		 * @brief This is only accessed from within interrupts.
		 * 
		 * This is only accessed by a single control flow, does not need to be volatile
		 */
		static inline Address m_SavedData{Address::Undefined};
		/**
		 * 
		 */
		template <typename EP_t, Direction current_direction>
		ALWAYS_INLINE static void Action( EP_t EP, USBActionData data ) noexcept
		{
			switch ( data.Action )
			{
			case USBAction::Enable:
				Enable();
				break;
			case USBAction::Disable:
				Disable();
				break;
			case USBAction::Connect:
				Connect();
				break;
			case USBAction::Disconnect:
				Disconnect();
				break;
			case USBAction::SetAddress:
				m_SavedData = data.Data.DeviceAddress;
				[[fallthrough]];
			case USBAction::Status:
				TriggerStatusStage( EP, current_direction );
				break;
			case USBAction::EndStatus:
				EndStatusStage( EP );
				break;
			case USBAction::ClearStatusFlag:
				ClearStatusFlag( EP );
				break;
			case USBAction::Stall:
				TriggerStall( EP, current_direction );
				break;
			case USBAction::ReceiveNAK:
				ReceiveNAK( EP );
				break;
			case USBAction::ReceiveStall:
				ReceiveStall( EP );
				break;
			case USBAction::ReceiveValid:
			case USBAction::ReceiveRaw:
				ReceiveValid( EP );
				break;
			case USBAction::TransmitNAK:
				TransmitNAK( EP );
				break;
			case USBAction::TransmitStall:
				TransmitStall( EP );
				break;
			case USBAction::TransmitValid:
				TransmitValid( EP );
				break;
			case USBAction::TransmitEmpty:
				TransmitEmpty( EP );
				break;
			case USBAction::TransmitComplete:
				TransmitComplete( EP );
				break;
			case USBAction::TransmitRaw:
			{
				TransmitTriggerRaw( EP, data.Data.Transmit );
				break;
			}
			default:
				break;
			}
		}
		/**
		 * 
		 */
		ALWAYS_INLINE static bool OtherAction( USBActionData data ) noexcept
		{
			if (data.Action == USBAction::UseOtherEndpoint)
			{
				GetEndpoint
				(
					data.Data.Other.Endpoint, 
					[&]( auto EP )
					{
						Action<decltype(EP), Direction::Unknown>(EP, *data.Data.Other.DataPtr);
					}
				);
				return true;
			}
			return false;
		}
		/**
		 * 
		 */
		template <typename EP_t, Direction CurrentDir>
		static void ActionBound( USBActionData data ) noexcept
		{
			if ( not OtherAction( data ) )
			{
				Action<EP_t, CurrentDir>( EP_t{}, data );
			}
		}
		ALWAYS_INLINE static void ActionDoNothing( USBActionData data ) noexcept {}
		/**
		 * @brief 
		 * 
		 * @tparam N 
		 * @tparam Settings 
		 * @param settings 
		 * @return ALWAYS_INLINE ConfigureEndpoints 
		 */
		template <uint8_t ... N, typename ...Settings>
		ALWAYS_INLINE static void ConfigureEndpoints( Settings ... settings ) noexcept
		{
			if constexpr  ( sizeof...( Settings ) == 0 )
			{
				ConfigureEndpointsImpl( Endpoint<0u, EndpointDirection::IN_OUT, 64u, EndpointType::Control, ControlKind::Normal>{} );
			}
			else
			{
				ConfigureEndpointsImpl( settings ... );
			}
		}
		/**
		 * @brief Triggers a system usb power on reset.
		 * 
		 * Sets up the following:
		 * 	-#	A peripheral reset.
		 * 	-#	BTABLE location.
		 * 	-#	Interrupt flags ALL enabled
		 * 
		 * @todo Add selectable masks based on the USBModule kernal.
		 */
		ALWAYS_INLINE static void SystemAndPowerOnReset() noexcept
		{
			using namespace USBGeneral;
			/**
			 * The analog part of the device related to the USBModule transceiver must be switched on
			 * using the PDWN bit in CNTR register, which requires a special handling. This bit is intended
			 * to switch on the internal voltage references that supply the port transceiver.
			 * 
			 * 1 - This performs full reset and clears the PDWN bit.
			 * 2 - Clearing the ISTR register then removes any spurious pending interrupt before any other 
			 * 3 - macrocell operation is enabled.
			 */
			CNTR{}.Set( USBGeneral::CNTR_FRES_MASK );
			CNTR{}.Set( 0 );
			/**
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * At system reset, the microcontroller must initialize all required registers and the packet
			 * buffer description table, to make the USBModule peripheral able to properly generate interrupts and
			 * data transfers.
			 * 
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * Endpoint Registers
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * - USBModule_EP0R	:	USBModule endpoint 0 register
			 * - USBModule_EP1R	:	USBModule endpoint 1 register
			 * - USBModule_EP2R	:	USBModule endpoint 2 register
			 * - USBModule_EP3R	:	USBModule endpoint 3 register
			 * - USBModule_EP4R	:	USBModule endpoint 4 register
			 * - USBModule_EP5R	:	USBModule endpoint 5 register
			 * - USBModule_EP6R	:	USBModule endpoint 6 register
			 * - USBModule_EP7R	:	USBModule endpoint 7 register
			 * 
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * Configuration and flag registers
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * - USBModule_CNTR	:	USBModule control register
			 * - USBModule_ISTR	:	USBModule interrupt status register
			 * - USBModule_FNR	:	USBModule frame number register
			 * - USBModule_DADDR	:	USBModule device address
			 * - USBModule_BTABLE	:	Buffer table address
			 * - USBModule_LPMCSR	:	LPM control and status register
			 * - USBModule_BCDR	:	Battery charging detector
			 * 
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * Set the BTABLE offset to zero, why would anybody want different.
			 * This feature seems like a anti-feature.
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 */
			BTABLE{} = 0u;
			/**
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 * Enable ALL interrupts
			 * ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
			 */
			/**
			 * Clear all resets
			 */
			ISTR{}.Set( 0 );
			CNTR{}.Set
			(
				CNTR_CTRM_MASK  | CNTR_ERRM_MASK 	|
				CNTR_ESOFM_MASK | CNTR_RESETM_MASK 	| 
				CNTR_WKUPM_MASK | CNTR_SUSPM_MASK 	| 
				CNTR_SOFM_MASK
			);
		}
		template <typename Systick>
		ALWAYS_INLINE static ModeBCD BatteryChargeDetect() noexcept
		{
			using namespace USBGeneral;
			/**
			 * @brief Enable the [b]attery [c]harge [d]etect system
			 * 
			 * RAII module ensures that it is disabled on all exit paths.
			 */
			General::RAII bcd_en
			{
				[]
				{
					BCDR{}.BCDEN() = true;
					Systick::Wait( 100 );
				},
				[]
				{
					BCDR{}.BCDEN() = false;
				}
			};
			/**
			 * @brief Enable the [p]rimary [d]etection mode
			 * 
			 * RAII module ensures that it is disabled on all exit paths.
			 */
			enum class PrimaryType
			{
				ProprietryOrPS2,
				SDP,
				Secondary
			};
			/**
			 * ...
			 */
			PrimaryType primary_detection = [&] () noexcept -> PrimaryType
			{
				General::RAII raii
				{
					[]
					{
						BCDR{}.PDEN() = true;
						Systick::Wait( 100 );
					},
					[]
					{
						BCDR{}.PDEN() = false;
					}
				};
				return ( BCDR{}.PDET().Get() ) ? PrimaryType::Secondary : ( BCDR{}.PS2DET().Get() ? PrimaryType::ProprietryOrPS2 : PrimaryType::SDP );
			}();
			/**
			 * ...
			 */
			switch (primary_detection)
			{
			case PrimaryType::Secondary:
			{
				/**
				 * Start secondary detection to check connection
				 */
				bool secondary_detection = [] () noexcept
				{
					General::RAII raii
					{
						[]
						{
							BCDR{}.SDEN() = true;
						},
						[]
						{
							BCDR{}.SDEN() = false;
						}
					};
					Systick::Wait( 100 );
					return BCDR{}.SDET().Get();
				}();
				/**
				 * DCP : [D]edicated [C]harging [P]ort
				 * CDP : [C]harging [D]ownstream [P]ort
				 */
				return secondary_detection ? ModeBCD::DCP : ModeBCD::CDP;
			}
			case PrimaryType::ProprietryOrPS2:
				return ModeBCD::ProprietryOrPS2;
			case PrimaryType::SDP:
				return ModeBCD::SDP;
			}
		}
		ALWAYS_INLINE static float BatteryChargePowerLimit(ModeBCD mode) noexcept
		{
			switch ( mode )
			{
			case ModeBCD::CDP:
			case ModeBCD::DCP:
				return ( 5.0f * 1.5f );
			case ModeBCD::SDP:
				return ( 5.0f * 0.5f );
			default:
				break;
			}
			/**
			 * This is the minimum unit power for a USB device.
			 * This is what USB hubs provide by default.
			 */
			return ( 5.0f * 0.1f );
		}
		/**
		 * 
		 */
		USBModuleBase() noexcept : 
			USBModule_DN{ Mode::Alternate, Speed::VeryHigh, Resistors::None, Type::PushPull },
			USBModule_DP{ Mode::Alternate, Speed::VeryHigh, Resistors::None, Type::PushPull }
		{}
	};
	/**
	 * @brief The USB module manager class for the USB peripheral.
	 * 
	 * @note USBModule clock must be initialised as HSE before using this class.
	 */
	template <typename Base, typename EventCallback, typename RXCallback>
	struct USBModule : 
		Base,
		System::Interrupt<USBModule<Base, EventCallback, RXCallback>, System::InterruptSource::eUSB>
	{
		using EventCallback_t	 	= General::StaticLambdaWrapper<EventCallback, 	USBModule>;
		using ReceiveCallback_t 	= General::StaticLambdaWrapper<RXCallback, 		USBModule>;
		//
		EventCallback_t		m_ActionCallback;
		ReceiveCallback_t 	m_ReceiveCallback;
	public:
		/**
		 * Configure the pins:
		 *      In Alternate mode
		 *      At maximum speed
		 *      With no pullup resistors
		 *      With push-pull drive
		 */
		template < typename EV, typename RX >
		USBModule( EV && action_callback, RX && recieve_callback ) noexcept : 
			m_ActionCallback	{ std::forward<EV>( action_callback  ) },
			m_ReceiveCallback 	{ std::forward<RX>( recieve_callback ) }
		{
			Base::SystemAndPowerOnReset();
		}
		template < typename EV, typename RX >
		USBModule( General::Type<Base>, EV && action_callback, RX && recieve_callback ) noexcept : 
			USBModule
			{
				std::forward<EV>( action_callback  ),
				std::forward<RX>( recieve_callback )
			}
		{}
		/**
		 * Receives data OUT in for normal operation (not a setup transaction)
		 */
		template <typename EP_t>
		ALWAYS_INLINE static void CTR_Receive_Interrupt( EP_t EP, USBReceiveType type ) noexcept
		{
			auto data{ USBGeneral::GetPacketMemoryArea()->ReceiveData( EP ) };
			/**
			 * 
			 */
			if ( Base::IsStatusStage( EP ) )
			{
				Base::ClearStatusFlag( EP );
				/**
				 * 
				 */
				EventCallback_t::Run
				(
					EP_t::Number,
					Base::template ActionBound<EP_t, Direction::OUT>,
					USBEvent::StatusComplete
				);
			}
			else
			{
				/**
				 * An OUT/RX transaction was completed
				 */
				ReceiveCallback_t::Run
				(
					EP_t::Number,
					Base::template ActionBound<EP_t, Direction::OUT>,
					type, 
					data
				);
			}
		}
		/**
		 * The standard Receive for the CTR_Interrupt
		 * Transmit is an IN transaction
		 */
		template <typename EP_t>
		ALWAYS_INLINE static void CTR_Transmit_Interrupt( EP_t EP ) noexcept
		{
			/**
			 * This creates a common location for the event callback.
			 */
			auto event_callback = []( USBEvent event )
			{
				EventCallback_t::Run
				(
					EP_t::Number,
					Base::template ActionBound<EP_t, Direction::IN>,
					event
				);
			};
			/**
			 * 
			 */
			if ( Base::IsStatusStage( EP ) )
			{
				/**
				 * Set the device address
				 * The Set Address command has an IN status stage.
				 */
				if ( Base::m_SavedData != Address::Undefined )
				{
					Base::SetAddress( Base::m_SavedData );
					Base::m_SavedData = Address::Undefined;
				}
				/**
				 * 
				 */
				Base::ClearStatusFlag( EP );
				event_callback( USBEvent::StatusComplete );
			}
			else
			{
				/**
				 * An IN/TX transaction was completed
				 */
				event_callback( USBEvent::TransmitComplete );
			}
		}
		/**
		 * Route the interrupt to the correct handler above
		 * this function also handles the clearing of the flags
		 */
		ALWAYS_INLINE static void CTR_Interrupt() noexcept
		{
			using namespace USBGeneral;
			/**
			 * Use the correct endpoint register
			 */
			Base::GetCurrentEndpoint
			(
				[]( auto EP ) noexcept
				{
					switch ( Base::CurrentTransferType( EP ) )
					{
					/**
					 * A transmit (IN) transaction was completed.
					 */
					case USBTransferType::IN:
					{
						Base::ClearTransmitCTR( EP );
						CTR_Transmit_Interrupt( EP );
						break;
					}
					/**
					 * A recieve interrupt (OUT) was completed.
					 */
					case USBTransferType::OUT:
					{
						Base::ClearReceiveCTR( EP );
						CTR_Receive_Interrupt( EP, USBReceiveType::OUT );
						break;
					}
					/**
					 * A recieve interrupt (SETUP) was completed.
					 * Parse setup request and respond as nessary
					 */
					case USBTransferType::SETUP:
					{
						CTR_Receive_Interrupt( EP, USBReceiveType::SETUP );
						Base::ClearReceiveCTR( EP );
						break;
					}
					/**
					 * This occurs if the flags in the registers are in an incompatible configuration
					 *  How is this possible
					 * 	Add a handler for this.
					 */
					default: break;
					}
				}
			);
		}
		/**
		 * Basic event, endpoint agnoistic
		 */
		static void BasicEvent(USBEvent event) noexcept
		{
			EventCallback_t::Run( 0, &Base::ActionDoNothing, event );
		}
		ALWAYS_INLINE static void SUSP_Interrupt() noexcept
		{
			using namespace USBGeneral;
			/**
			 * Why suspend if its already suspended
			 */
			if ( !CNTR{}.FSUSP() )
			{
				BasicEvent( USBEvent::SuspendEvent );
				CNTR{}.FSUSP()  = true;
				CNTR{}.LPMODE() = true;
			}
		}
		ALWAYS_INLINE static void WKUP_Interrupt() noexcept
		{
			using namespace USBGeneral;
			/**
			 * Only wake if the device is suspended
			 */
			if ( CNTR{}.FSUSP() )
			{
				/**
				 * Decode the reason for wakeup condition
				 */	
				switch( Base::DecodeWakeup() )
				{
				case USBWakeupEvent::RootReset:
				case USBWakeupEvent::RootResume:
					CNTR{}.Set
					(
						CNTR_CTRM_MASK  | CNTR_ERRM_MASK 	|
						CNTR_ESOFM_MASK | CNTR_RESETM_MASK 	| 
						CNTR_WKUPM_MASK | CNTR_SUSPM_MASK 	| 
						CNTR_SOFM_MASK
					);
					/**
					 * Root reset
					 * Root resume
					 * Re-enable the system
					 */
					BasicEvent( USBEvent::WakeupEvent );
					break;
				default:
					SUSP_Interrupt();
					break;
				}
			}
		}
		/**
		 * When this event occurs, the USBModule peripheral is put in the same conditions it is left by the
		 * system reset after the initialization described in the previous paragraph: communication is
		 * disabled in all endpoint registers (the USBModule peripheral will not respond to any packet).
		 *
		 * As a response to the USBModule reset event, the USBModule function must be enabled, having as USBModule
		 * address 0, implementing only the default control endpoint (endpoint address 0). 
		 *
		 * This is accomplished by setting the Enable Function (EF) bit of the USBModule_DADDR register 
		 * and initializing the EP0R register and its related packet buffers accordingly.
		 */
		ALWAYS_INLINE static void RESET_Interrupt() noexcept
		{
			BasicEvent( USBEvent::EndpointSetup );
			Base::Enable();
		}
		ALWAYS_INLINE static void SOF_Interrupt() 	 noexcept 
		{
			//BasicEvent( USBEvent::StartOfFrame );
		}
		ALWAYS_INLINE static void ESOF_Interrupt() 	 noexcept {}
		ALWAYS_INLINE static void ERR_Interrupt()	 noexcept {}
		ALWAYS_INLINE static void PMAOVR_Interrupt() noexcept {}
		ALWAYS_INLINE static void L1REQ_Interrupt()  noexcept {}
		/**
		 * This services and sorts ALL USBModule interrupts into the above handlers
		 */
		static void Interrupt()
		{
			using namespace USBGeneral;
			//
			if ( CNTR{}.SOFM() and ISTR{}.SOF() )
			{
				ISTR{}.SOF().Clear();
				SOF_Interrupt();
			}
			if ( CNTR{}.ESOFM() and ISTR{}.ESOF() )
			{
				ISTR{}.ESOF().Clear();
				ESOF_Interrupt();
			}
			while ( CNTR{}.CTRM() and ISTR{}.CTR() )
			{
				CTR_Interrupt();
			}
			if ( CNTR{}.ERRM() and ISTR{}.ERR() )
			{
				ERR_Interrupt();
				ISTR{}.ERR().Clear();
			}
			if ( CNTR{}.PMAOVRM() and ISTR{}.PMAOVR() )
			{
				PMAOVR_Interrupt();
				ISTR{}.PMAOVR().Clear();
			}
			if ( CNTR{}.L1REQM() and ISTR{}.L1REQ() )
			{
				L1REQ_Interrupt();
				ISTR{}.L1REQ().Clear();
			}
			if ( CNTR{}.SUSPM() and ISTR{}.SUSP() )
			{
				SUSP_Interrupt();
				ISTR{}.SUSP().Clear();
			}
			if ( CNTR{}.WKUPM() and ISTR{}.WKUP() )
			{
				WKUP_Interrupt();
				ISTR{}.WKUP().Clear();
			}
			if ( CNTR{}.RESETM() and ISTR{}.RESET() )
			{
				RESET_Interrupt();
				ISTR{}.RESET().Clear();
			}
		}
	};
	/**
	 * 
	 */
	template <typename Kernal, typename USBModule_DN, typename USBModule_DP, typename EventCallback, typename RXCallback>
	USBModule(General::Type<USBModuleBase<Kernal, USBModule_DN, USBModule_DP>>, EventCallback e, RXCallback) -> USBModule<USBModuleBase<Kernal, USBModule_DN, USBModule_DP>, EventCallback, RXCallback>;
}