#pragma once

#include "ParseResult.hpp"
#include <array>

namespace Parser
{
	/**
	 * @brief A Parser class that always returns a 0 length true parser result.
	 * 
	 */
	struct Fallback
	{
		template <std::size_t N>
		constexpr ParseResult operator()( std::array<char, N> const &, std::size_t = 0u, std::size_t = 0u ) const noexcept
		{
			return true;
		}
	};
}