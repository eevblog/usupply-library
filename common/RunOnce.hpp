#pragma once

namespace General
{
	/**
	 * @brief This class manages one time construction objects such as peripherals.
	 * 
	 * The power for a peripheral is an example of a one time construction object.
	 * Powering on a peripheral a second time does nothing.
	 * 
	 * The reference counts the construction and only runs the constructor when nessary.
	 * 
	 * @tparam T A type that has a static function Construct and another called Destruct.
	 */
	template <typename T>
	class RunOnce
	{
	private:
		inline static unsigned Count = 0;
		static bool Construct() noexcept
		{
			bool const output = ( Count == 0 );
			++Count;
			return output;
		}
		static bool Destruct() noexcept 
		{
			return ( --Count == 0 );
		}
	public:
		/**
		 * @brief Construct a new Run Once object
		 */
		RunOnce() noexcept
		{
			if ( Construct() )
				T::Construct();
		}
		/**
		 * @brief Destroy the Run Once object
		 */
		~RunOnce()
		{
			if ( Destruct() )
				T::Destruct();
		}
	};
}