#pragma once

#include "Meta.hpp"

namespace General
{
	/**
	 * @brief This is used for metaprogramming, its purpose it to hide all inherited member functions behind a Get call.
	 * 
	 * @tparam T The type to hide.
	 */
	template <typename T>
	struct Hide : private T
	{
		using T::T;
		using T::operator=;
		/**
		 * @brief Returns a non-const reference to the base class.
		 * 
		 * @return T& The base class. 
		 */
		constexpr T & Get() noexcept
		{
			return static_cast<T&>(*this);
		}
		/**
		 * @brief Returns a const reference to the base class.
		 * 
		 * @return T const & The base class. 
		 */
		constexpr T const & Get() const noexcept
		{
			return static_cast<T const &>(*this);
		}
	};
	//
	template <typename T>
	Hide(T&&)->Hide<decay_rvalue<T>>;
}