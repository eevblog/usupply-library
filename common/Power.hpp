#pragma once
#include "RunOnce.hpp"

namespace General
{
	/**
	 * @brief A utiltity that runs the powerup function for a module only once.
	 * 
	 * @tparam Module The module to powerup.
	 */
	template <typename Module>
	using ModulePower = General::RunOnce<Module>;
}