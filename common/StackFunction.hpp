#pragma once
#include "StackObject.hpp"

namespace General
{
    //
    template <size_t BufferSize, typename>
    class StackFunction;
    /**
     * @brief A function wrapper that stores bound data on the stack.
     * 
     * @tparam BufferSize The buffer size for bound data.
     * @tparam Ret The return value of the function.
     * @tparam Args The arguments of the function.
     */
    template <size_t BufferSize, typename Ret, typename ... Args>
    class StackFunction<BufferSize, Ret(Args...) noexcept> : StackObject<BufferSize>
    {
        using stack_object_t = StackObject<BufferSize>;
        Ret ( *m_Runner )( const void *, Args ... ) noexcept = nullptr;
    public:
        /**
         * @brief Construct a new Stack empty Function object
         * 
         */
        StackFunction() = default;
        /**
         * @brief Constructs the function object into the stack object.
         * 
         * @tparam F The function type.
         * @tparam std::decay_t<F> Decayed function type.
         * @param function The function object.
         */
        template <typename F, typename D = std::decay_t<F>>
        void Construct( F && function ) noexcept
        {
            static_assert(sizeof(D) <= BufferSize, "The function doesn't fit in storage.");
            //
            stack_object_t::Construct
            (
                Type<D>{}, 
                std::forward<F>( function )
            );
            //
            m_Runner = []( const void * f, Args ... args ) noexcept -> Ret
            {
                if constexpr ( std::is_void_v<Ret> )
                {
                    ( *static_cast<F const *>( f ) )( args ... );
                }
                else
                {
                    return ( *static_cast<F const *>( f ) )( args ... );
                }
            };
        }
        /**
         * @brief Runs the function destructor (if it has objects bound to it)
         */
        void Destruct() noexcept
        {
            stack_object_t::Destruct();
        }
        /**
         * @brief Construct a new Stack Function object
         * 
         * @tparam F The function type.
         * @tparam std::decay_t<F> The resultant function type.
         * @param function The function.
         */
        template <typename F, typename D = std::decay_t<F>>
        StackFunction( F && function ) noexcept
        {
            Construct( std::forward<F>( function ) );
            static_assert(sizeof(D) <= BufferSize, "The function doesn't fit in storage.");
        }
        /**
         * @brief Checks if the function object is valid.
         * 
         * @return true The object is a valid function.
         * @return false The function is not valid.
         */
        bool Valid() const noexcept
        {
            return !!m_Runner;
        }
        /**
         * @brief Checks if the function object is valid.
         * 
         * @return true The object is a valid function.
         * @return false The function is not valid.
         */
        operator bool() const noexcept
        {
            return Valid();
        }
        /**
         * @brief Runs the function.
         * 
         * @param args The function arguments.
         * @return Ret The return value of the function.
         */
        Ret operator()( Args ... args ) noexcept
        {
            return m_Runner( stack_object_t::Ptr(), args ... );
        }
    };
}