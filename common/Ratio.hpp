#pragma once

namespace General
{
	/**
	 * @brief Represents a number that is between 0 and 1.
	 * 
	 * @tparam T the underlying type to represent a decimal number.
	 */
	template <typename T = float>
	class Ratio
	{
	private:
		T const m_Value;
	public:
		/**
		 * @brief Create the ratio from a number.
		 */
		constexpr Ratio( T const & input ) noexcept :
		    m_Value( input )
		{}
		/**
		 * @brief Implicit convertion to the underlying type.
		 * 
		 * The result is clamped between 0 and 1.
		 * 
		 * @return T The ratio.
		 */
		constexpr operator T() const noexcept
		{
			if ( m_Value > (T)1 ) return (T)1;
			if ( m_Value < (T)0 ) return (T)0;
			return m_Value;
		}
	};
}