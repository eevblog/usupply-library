#pragma once

#include "Math.hpp"
#include "Config.hpp"
#include "Constants.hpp"
#include "RegistersRCC.hpp"

#include <cstdint>

namespace Peripherals
{	
	enum class Peripheral : std::uint32_t
	{
		ClockTSC,
		ClockGPIOF,
		ClockGPIOE,
		ClockGPIOD,
		ClockGPIOC,
		ClockGPIOB,
		ClockGPIOA,
		ClockCRC,
		ClockFLITF,
		ClockSRAM,
		ClockDMA2,
		ClockDMA1,
		ClockDMA = ClockDMA1,
		ClockDebug,
		ClockTIM17,
		ClockTIM16,
		ClockTIM15,
		ClockSPI1,
		ClockTIM1,
		ClockADC,
		ClockUSART1,
		ClockUSART6,
		ClockUSART7,
		ClockUSART8,
		ClockSYSCFGCOMP,
		ClockHDMI_CEC,
		ClockDAC,
		ClockPWR,
		ClockCRS,
		ClockCAN,
		ClockUSB,
		ClockI2C2,
		ClockI2C1,
		ClockUSART5,
		ClockUSART4,
		ClockUSART3,
		ClockUSART2,
		ClockSPI2,
		ClockWWDG,
		ClockTIM14,
		ClockTIM7,
		ClockTIM6,
		ClockTIM3,
		ClockTIM2,
	};
	/**
	 * @brief A manager class for the RCC peripheral.
	 * 
	 * This class is used to enable or disable any peripheral on the micro.
	 * 
	 * @tparam module 
	 */
	template <Peripheral module>
	struct RCCPeripheral
	{
		/**
		 * @brief Turns on or off the clock to a peripheral.
		 * 
		 * @param power Whether the state is on or off.
		 */
		static void Power(RCCGeneral::ClockState const power = RCCGeneral::ClockState::On) noexcept
		{
			using namespace RCCGeneral;
			
			if constexpr ( module == Peripheral::ClockTSC )
				AHBENR{}.TSCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOF        )
				AHBENR{}.GPIOFEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOE        )
				AHBENR{}.GPIOEEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOD        )
				AHBENR{}.GPIODEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOC        )
				AHBENR{}.GPIOCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOB        )
				AHBENR{}.GPIOBEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockGPIOA        )
				AHBENR{}.GPIOBEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockCRC          )
				AHBENR{}.CRCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockFLITF        )
				AHBENR{}.FLITFEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSRAM         )
				AHBENR{}.SRAMEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDMA2         )
				AHBENR{}.DMA2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDMA1         )
				AHBENR{}.DMAEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDebug        )
				APB2ENR{}.DBGMCUEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM17        )
				APB2ENR{}.TIM17EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM16        )
				APB2ENR{}.TIM16EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM15        )
				APB2ENR{}.TIM15EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSPI1         )
				APB2ENR{}.SPI1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM1         )
				APB2ENR{}.TIM1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockADC          )
				APB2ENR{}.ADCEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART1       )
				APB2ENR{}.USART1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART6       )
				APB2ENR{}.USART6EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART7       )
				APB2ENR{}.USART7EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART8       )
				APB2ENR{}.USART8EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSYSCFGCOMP   )
				APB2ENR{}.SYSCFGCOMPEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockHDMI_CEC     )
				APB1ENR{}.CECEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockDAC          )
				APB1ENR{}.DACEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockPWR          )
				APB1ENR{}.PWREN() = (bool)power;
			if constexpr ( module == Peripheral::ClockCRS          )
				APB1ENR{}.CRSEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockCAN          )
				APB1ENR{}.CANEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSB          )
				APB1ENR{}.USBEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockI2C2         )
				APB1ENR{}.I2C2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockI2C1         )
				APB1ENR{}.I2C1EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART5       )
				APB1ENR{}.USART5EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART4       )
				APB1ENR{}.USART4EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART3       )
				APB1ENR{}.USART3EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockUSART2       )
				APB1ENR{}.USART2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockSPI2         )
				APB1ENR{}.SPI2EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockWWDG         )
				APB1ENR{}.WWDGEN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM14        )
				APB1ENR{}.TIM14EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM7         )
				APB1ENR{}.TIM7EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM6         )
				APB1ENR{}.TIM6EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM3         )
				APB1ENR{}.TIM3EN() = (bool)power;
			if constexpr ( module == Peripheral::ClockTIM2         )
				APB1ENR{}.TIM2EN() = (bool)power;
		}
		/**
		 * @brief Resets a peripheral to its default state.
		 */
		static void Reset() noexcept
		{
			using namespace RCCGeneral;
			
			if constexpr ( module == Peripheral::ClockGPIOF        )
				AHBRSTR{}.GPIOFRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOE        )
				AHBRSTR{}.GPIOERST() = true;
			if constexpr ( module == Peripheral::ClockGPIOD        )
				AHBRSTR{}.GPIODRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOC        )
				AHBRSTR{}.GPIOCRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOB        )
				AHBRSTR{}.GPIOBRST() = true;
			if constexpr ( module == Peripheral::ClockGPIOA        )
				AHBRSTR{}.GPIOBRST() = true;
			if constexpr ( module == Peripheral::ClockCRC          )
				APB1RSTR{}.CRSRST() = true;
			if constexpr ( module == Peripheral::ClockDebug        )
				APB2RSTR{}.DBGMCURST() = true;
			if constexpr ( module == Peripheral::ClockTIM17        )
				APB2RSTR{}.TIM17RST() = true;
			if constexpr ( module == Peripheral::ClockTIM16        )
				APB2RSTR{}.TIM16RST() = true;
			if constexpr ( module == Peripheral::ClockTIM15        )
				APB2RSTR{}.TIM15RST() = true;
			if constexpr ( module == Peripheral::ClockSPI1         )
				APB2RSTR{}.SPI1RST() = true;
			if constexpr ( module == Peripheral::ClockTIM1         )
				APB2RSTR{}.TIM1RST() = true;
			if constexpr ( module == Peripheral::ClockADC          )
				APB2RSTR{}.ADCRST() = true;
			if constexpr ( module == Peripheral::ClockUSART1       )
				APB2RSTR{}.USART1RST() = true;
			if constexpr ( module == Peripheral::ClockUSART6       )
				APB2RSTR{}.USART6RST() = true;
			if constexpr ( module == Peripheral::ClockUSART7       )
				APB2RSTR{}.USART7RST() = true;
			if constexpr ( module == Peripheral::ClockUSART8       )
				APB2RSTR{}.USART8RST() = true;
			if constexpr ( module == Peripheral::ClockSYSCFGCOMP   )
				APB2RSTR{}.SYSCFGRST() = true;
			if constexpr ( module == Peripheral::ClockHDMI_CEC     )
				APB1RSTR{}.CECRST() = true;
			if constexpr ( module == Peripheral::ClockDAC          )
				APB1RSTR{}.DACRST() = true;
			if constexpr ( module == Peripheral::ClockPWR          )
				APB1RSTR{}.PWRRST() = true;
			if constexpr ( module == Peripheral::ClockCRS          )
				APB1RSTR{}.CRSRST() = true;
			if constexpr ( module == Peripheral::ClockCAN          )
				APB1RSTR{}.CANRST() = true;
			if constexpr ( module == Peripheral::ClockUSB          )
				APB1RSTR{}.USBRST() = true;
			if constexpr ( module == Peripheral::ClockI2C2         )
				APB1RSTR{}.I2C2RST() = true;
			if constexpr ( module == Peripheral::ClockI2C1         )
				APB1RSTR{}.I2C1RST() = true;
			if constexpr ( module == Peripheral::ClockUSART5       )
				APB1RSTR{}.USART5RST() = true;
			if constexpr ( module == Peripheral::ClockUSART4       )
				APB1RSTR{}.USART4RST() = true;
			if constexpr ( module == Peripheral::ClockUSART3       )
				APB1RSTR{}.USART3RST() = true;
			if constexpr ( module == Peripheral::ClockUSART2       )
				APB1RSTR{}.USART2RST() = true;
			if constexpr ( module == Peripheral::ClockSPI2         )
				APB1RSTR{}.SPI2RST() = true;
			if constexpr ( module == Peripheral::ClockWWDG         )
				APB1RSTR{}.WWDGRST() = true;
			if constexpr ( module == Peripheral::ClockTIM14        )
				APB1RSTR{}.TIM14RST() = true;
			if constexpr ( module == Peripheral::ClockTIM7         )
				APB1RSTR{}.TIM7RST() = true;
			if constexpr ( module == Peripheral::ClockTIM6         )
				APB1RSTR{}.TIM6RST() = true;
			if constexpr ( module == Peripheral::ClockTIM3         )
				APB1RSTR{}.TIM3RST() = true;
			if constexpr ( module == Peripheral::ClockTIM2         )
				APB1RSTR{}.TIM2RST() = true;
		}
	};
	/**
	 * @brief 
	 * 
	 * @tparam Item 
	 */
	template <Peripheral Item>
	struct GeneralPowerKernal
	{
		static constexpr Peripheral Current = Item;
		using RCCPeripheral_t = RCCPeripheral<Item>;
				
		static void Construct() noexcept
		{
			RCCPeripheral_t::Power(RCCGeneral::ClockState::On);
		}
		static void Destruct() noexcept
		{
			RCCPeripheral_t::Power(RCCGeneral::ClockState::Off);
		}
	};
	/**
	 * @brief 
	 * 
	 * @tparam module 
	 */
	template <RCCGeneral::Clocks module>
	struct RCCClock
	{
		/**
		 * @brief 
		 * 
		 * @param power 
		 */
		static void Power(RCCGeneral::ClockState const power = RCCGeneral::ClockState::On) noexcept
		{
			using namespace RCCGeneral;
			if (power == ClockState::On)
			{
				if constexpr (module == Clocks::HSE) 	CR{}.	EnableHSE();
				if constexpr (module == Clocks::HSI)	CR{}.	EnableHSI();
				if constexpr (module == Clocks::HSI14) 	CR2{}.	EnableHSI14();
				if constexpr (module == Clocks::HSI48)	CR2{}.	EnableHSI48();
				if constexpr (module == Clocks::LSE)	BDCR{}.	EnableLSE();
				if constexpr (module == Clocks::LSI)	CSR{}.	EnableLSI();
			}
			else
			{				
				if constexpr (module == Clocks::HSE) 	CR{}.	DisableHSE();
				if constexpr (module == Clocks::HSI)	CR{}.	DisableHSI();
				if constexpr (module == Clocks::HSI14) 	CR2{}.	DisableHSI14();
				if constexpr (module == Clocks::HSI48)	CR2{}.	DisableHSI48();
				if constexpr (module == Clocks::LSE)	BDCR{}.	DisableLSE();
				if constexpr (module == Clocks::LSI)	CSR{}.	DisableLSI();
			}
		}
	};
	/**
	 * @brief A manager class for the system clock.
	 * 
	 * This sets up:
	 * @li PLL
	 * @li Clock sources.
	 * @li Clock tree calculations.
	 * 
	 * @tparam Source The main clock source.
	 * @tparam PCLKDivision The division for PCLK.
	 * @tparam HCLKDivision The division for HCLK.
	 * @tparam PLLMultiple The multiplier for the PLL.
	 * @tparam PLLDivision The divider for the PLL.
	 * @tparam UseAsSystemClock Use the clock as the system clock (instead of the default internal clock).
	 */
	template
	<
		RCCGeneral::Clocks 			Source,
		RCCGeneral::PCLKDivision 	PCLKDivision,
		RCCGeneral::HCLKDivision 	HCLKDivision,
		RCCGeneral::PLLMultiply		PLLMultiply,
		RCCGeneral::PLLDivision		PLLDivision,
		bool						UseAsSystemClock = true
	>
	class SystemClock
	{
	private:	
		using Clocks           	= RCCGeneral::Clocks;
		using PLLSource        	= RCCGeneral::PLLSource;
		using SystemClockSource = RCCGeneral::SystemClockSource;
		
		static void SetupPLL() noexcept
		{
			using namespace RCCGeneral;
		#ifdef STM32F072
			if constexpr( Source == Clocks::HSI_2 )	CFGR{}.SetPLLSource( PLLSource::HSI_2 );
			if constexpr( Source == Clocks::HSI   )	CFGR{}.SetPLLSource( PLLSource::HSI_PREDIV );
			if constexpr( Source == Clocks::HSI48 )	CFGR{}.SetPLLSource( PLLSource::HSI48_PREDIV );
			if constexpr( Source == Clocks::HSE   )	CFGR{}.SetPLLSource( PLLSource::HSE_PREDIV );
		#else
			if constexpr( Source == Clocks::HSI_2 )	CFGR{}.SetPLLSource( PLLSource::HSI_2 );
			if constexpr( Source == Clocks::HSE   )	CFGR{}.SetPLLSource( PLLSource::HSE_PREDIV );
		#endif
			/**
			 * Modify the multiply and division (only mutable when PLL disabled)
			 */
			if constexpr( Source == Clocks::HSE )
			{
				CFGR{}.SelectSystemClock( SystemClockSource::HSI );
				CR{}.BypassHSE();
			}
			CR{}.	DisablePLL();
			CFGR2{}.SetPLLDivision( PLLDivision );
			CFGR{}.	SetPLLMultiply( PLLMultiply );
			CR{}.	EnablePLL();
			/**
			 * Only enable the clock as system clock if that is requested...
			 */
			if constexpr (UseAsSystemClock)
			{
				CFGR{}.	SelectSystemClock( SystemClockSource::PLL );
			}
		}
		/**
		 * 
		 */
		static constexpr std::uint64_t SourceFrequency() noexcept
		{
			if constexpr (Source == Clocks::HSE) 	return ::System::HSEClock;
			if constexpr (Source == Clocks::HSI_2)	return ::System::HSIClock / 2;
			if constexpr (Source == Clocks::HSI)	return ::System::HSIClock;

			#ifdef STM32F072
			if constexpr (Source == Clocks::HSI48) 	return ::System::HSI48Clock;
			#endif
		}
		
		static constexpr std::uint64_t GetMultiple() noexcept
		{
			return (std::uint64_t)PLLMultiply + (uint64_t)2u;
		}

		static constexpr std::uint64_t GetDivider() noexcept
		{
			return (std::uint64_t)PLLDivision + (uint64_t)1u;
		}
		/**
		 * 
		 */
		static constexpr std::uint64_t HPREDivision() noexcept
		{
			switch( HCLKDivision )
			{
			case RCCGeneral::HCLKDivision::SYSCLK_1: 	return 1u;
			case RCCGeneral::HCLKDivision::SYSCLK_2: 	return 2u;
			case RCCGeneral::HCLKDivision::SYSCLK_4: 	return 4u;
			case RCCGeneral::HCLKDivision::SYSCLK_8: 	return 8u;
			case RCCGeneral::HCLKDivision::SYSCLK_16: 	return 16u;
			case RCCGeneral::HCLKDivision::SYSCLK_64: 	return 64u;
			case RCCGeneral::HCLKDivision::SYSCLK_128: 	return 128u;
			case RCCGeneral::HCLKDivision::SYSCLK_256: 	return 256u;
			default: return 0u;
			}
		}

		static constexpr std::uint64_t PPREDivision() noexcept
		{
			switch ( PCLKDivision )
			{
			case RCCGeneral::PCLKDivision::HCLK_1: 		return 1u;
			case RCCGeneral::PCLKDivision::HCLK_2: 		return 2u;
			case RCCGeneral::PCLKDivision::HCLK_4: 		return 4u;
			case RCCGeneral::PCLKDivision::HCLK_8: 		return 8u;
			case RCCGeneral::PCLKDivision::HCLK_16:		return 16u;
			default: return 0u;
			}
		}
		/**
		 * 
		 */
		RCCClock<Source> m_Clock;
	public:
		/**
		 * @brief Sets up the system clock.
		 */
		SystemClock() noexcept
		{
			using namespace RCCGeneral;

			// Set latency for high speed clock
			// @todo Make this dependant on the clock.
			FLASH->ACR = FLASH_ACR_LATENCY;
			
			// Enable selected clock
			m_Clock.Power();
			
			// Enable HCLK, PCLK
			CFGR{}.SetupPrescalers( HCLKDivision, PCLKDivision );
			
			// Enable PLL, no point if it is at the boot value of 1
			SetupPLL();
		}
		/**
		 * @brief The frequency of the system clock.
		 * 
		 * @return The system clock frequency.
		 */
		static constexpr std::uint64_t Frequency() noexcept
		{
			//Maximum multiple is 16, uint32 can store ~=4 billion, maximum clock is
			// 48MHz (48 x 16) million < 4000 million therefore this is proven safe even when this processor
			// is clocked stupidly
			return ::General::RoundedDivide<std::uint64_t>( ( SourceFrequency() * GetMultiple() ), GetDivider() );
		}
		/**
		 * @brief Get the frequency that feeds into timers.
		 * 
		 * @return The timer clock frequency.
		 */
		static constexpr std::uint64_t Timer() noexcept
		{
			return ::General::RoundedDivide<std::uint64_t>( Frequency(), (HPREDivision() * PPREDivision()) );
		}
	};
}