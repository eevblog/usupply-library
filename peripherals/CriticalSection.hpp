#pragma once
#include <cmsis_gcc.h>

namespace Peripherals
{
    /**
     * @brief Sets up a USB specific critcal section.
     * 
     * This data structure manages the disabling and restoring of interrupts for a USB specific critical section.
     */
    struct CriticalSection
    {
        /**
         * @brief Disables interrupts on construction.
         */
        CriticalSection() noexcept
        {
            __disable_irq();
        }
        /**
         * @brief Re-enables the interrupts on destruction.
         */
        ~CriticalSection()
        {
            __enable_irq(); 
        }
    };
}