#pragma once

#include "Meta.hpp"
#include "Register.hpp"
#include "RCC.hpp"
#include "stm32f0xx.h"
#include "Types.hpp"
#include "Math.hpp"
#include <cstdint>
#include <cstddef>

namespace Peripherals::I2CGeneral
{
	constexpr std::size_t BaseAddress(std::uint32_t channel) noexcept
	{
		switch(channel)
		{
			case 1: 	return I2C1_BASE;
			case 2: 	return I2C2_BASE;
			default:	return 0;
		};
	}
	constexpr System::InterruptSource GetInterruptSource(std::uint32_t channel) noexcept
	{
		switch(channel)
		{
			#ifdef STM32F072
			case 1:
				return System::InterruptSource::eI2C1;
			case 2:
				return System::InterruptSource::eI2C2;
			#endif
			#ifdef STM32F070x6
			default:
				return System::InterruptSource::eI2C1;
			#endif
		};
	}
	//
	template <std::uint32_t C, std::size_t A = 0x0>
	struct CR1
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
	private:
		// SMBus Category
		struct SMBus_t : base_t
		{
			using base_t::base_t;
			//
			// PEC enable
			auto PECEN		() noexcept { return base_t::template Actual<I2C_CR1_PECEN	>(); }
			//
			// SMBus alert enable
			auto ALERTEN	() noexcept { return base_t::template Actual<I2C_CR1_ALERTEN	>(); }
			//
			// SMBus Device Default address enable
			auto SMBDEN		() noexcept { return base_t::template Actual<I2C_CR1_SMBDEN	>(); }
			//
			// SMBus Host address enable
			auto SMBHEN		() noexcept { return base_t::template Actual<I2C_CR1_SMBHEN	>(); }
			//
			//
		};
		struct Filter_t : base_t
		{
			using base_t::base_t;
			//
			// Analog noise filter OFF
			auto ANFOFF		() noexcept { return base_t::template Actual<I2C_CR1_ANFOFF>(); }
			//
			// Digital noise filter
			auto DNF		() noexcept { return base_t::template Actual<I2C_CR1_DNF>(); }
		};
		struct Interrupt_t : base_t
		{
			using base_t::base_t;
			//
			// Error interrupts enable
			auto ERRIE		() noexcept { return base_t::template Actual<I2C_CR1_ERRIE	>(); }
			//
			// Transfer Complete interrupt enable
			auto TCIE		() noexcept { return base_t::template Actual<I2C_CR1_TCIE	>(); }
			//
			// STOP detection Interrupt enable
			auto STOPIE		() noexcept { return base_t::template Actual<I2C_CR1_STOPIE	>(); }
			//
			// Not acknowledge received Interrupt enable
			auto NACKIE		() noexcept { return base_t::template Actual<I2C_CR1_NACKIE	>(); }
			//
			// Address match Interrupt enable (slave only)
			auto ADDRIE		() noexcept { return base_t::template Actual<I2C_CR1_ADDRIE	>(); }
			//
			// RX Interrupt enable
			auto RXIE		() noexcept { return base_t::template Actual<I2C_CR1_RXIE	>(); }
			//
			// TX Interrupt enable
			auto TXIE		() noexcept { return base_t::template Actual<I2C_CR1_TXIE	>(); }
		};
		struct Config_t : base_t
		{
			using base_t::base_t;
			//
			// General call enable
			auto GCEN		() noexcept { return base_t::template Actual<I2C_CR1_GCEN	>(); }
			//
			// Wakeup from Stop mode enable
			auto WUPEN		() noexcept { return base_t::template Actual<I2C_CR1_WUPEN	>(); }
			//
			// Clock stretching disable
			auto NOSTRETCH	() noexcept { return base_t::template Actual<I2C_CR1_NOSTRETCH>(); }
			//
			// Slave byte control
			auto SBC		() noexcept { return base_t::template Actual<I2C_CR1_SBC		>(); }
			//
			// DMA reception requests enable
			auto RXDMAEN	() noexcept { return base_t::template Actual<I2C_CR1_RXDMAEN	>(); }
			//
			// DMA transmission requests enable
			auto TXDMAEN	() noexcept { return base_t::template Actual<I2C_CR1_TXDMAEN	>(); }
			//
			// Peripheral enable
			auto PE			() noexcept { return base_t::template Actual<I2C_CR1_PE		>(); }
			//
		};
	public:
		//
		SMBus_t 	SMBus()  	{ return {}; }
		Filter_t 	Filter() 	{ return {}; }
		Interrupt_t Interrupt() { return {}; }
		Config_t 	Config() 	{ return {}; }
		//
	};
	//
	template <std::uint32_t C, std::size_t A = 0x4>
	struct CR2
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
	private:
		struct SMBus_t : base_t
		{
			using base_t::base_t;
			//
			// Packet error checking byte
			auto PECBYTE() noexcept { return base_t::template Actual<I2C_CR2_PECBYTE>(); }
		};
		struct Config_t : base_t
		{
			using base_t::base_t;
			//
			// 10-bit address header only read direction (master receiver mode)
			auto HEAD10R() noexcept { return base_t::template Actual<I2C_CR2_HEAD10R>(); }
			//
			// 10-bit addressing mode (master mode)
			auto ADD10() noexcept { return base_t::template Actual<I2C_CR2_ADD10>(); }
		};
		struct Protocol_t : base_t
		{
			using base_t::base_t;
			//
			// Automatic end mode (master mode)
			auto AUTOEND() noexcept { return base_t::template Actual<I2C_CR2_AUTOEND>(); }
			//
			// Slave address (master mode)
			auto SADD() noexcept 	{ return base_t::template Actual<I2C_CR2_SADD>(); }
			//
			// Number of bytes (master mode)
			auto NBYTES() noexcept 	{ return base_t::template Actual<I2C_CR2_NBYTES>(); }
			//
			// NACK generation (slave mode)
			auto NACK() noexcept 	{ return base_t::template Actual<I2C_CR2_NACK>(); }
			//
			// Transfer direction (master mode)
			//	0: Master requests a write transfer.
			//	1: Master requests a read transfer.
			auto RD_WRN() noexcept { return base_t::template Actual<I2C_CR2_RD_WRN>(); }
			//
			// Stop generation (master mode)
			// 	0: No Stop generation.
			// 	1: Stop generation after current byte transfer.
			auto STOP() noexcept { return base_t::template Actual<I2C_CR2_STOP>(); }
			//
			// START: Start generation
			// 	0: No Start generation.
			// 	1: Restart/Start generation:
			auto START() noexcept { return base_t::template Actual<I2C_CR2_START>(); }
			//
			// NBYTES reload mode
			// 	0: The transfer is completed after the NBYTES data transfer (STOP or RESTART will follow).
			// 	1: The transfer is not completed after the NBYTES data transfer (NBYTES will be reloaded).
			// 	TCR flag is set when NBYTES data are transferred, stretching SCL low.
			auto RELOAD() noexcept { return base_t::template Actual<I2C_CR2_RELOAD>(); }
		};

	public:
		SMBus_t 	SMBus() 	{ return {}; }
		Config_t 	Config() 	{ return {}; }
		Protocol_t	Protocol()	{ return {}; }
	};
	//
	template <std::uint32_t C, std::size_t A = 0x8>
	struct OAR1 : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;
		//
		//
		auto OA1EN() 	noexcept { return base_t::template Actual<I2C_OAR1_OA1EN>(); 	}
		auto OA1MODE() 	noexcept { return base_t::template Actual<I2C_OAR1_OA1MODE>(); 	}
		auto OA1() 		noexcept { return base_t::template Actual<I2C_OAR1_OA1>(); 		}
		//
		//
	};
	//
	template <std::uint32_t C, std::size_t A = 0xC>
	struct OAR2 : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;
		//
		//
		auto OA2EN() 	noexcept { return base_t::template Actual<I2C_OAR2_OA2EN>(); 	}
		auto OA2MSK() 	noexcept { return base_t::template Actual<I2C_OAR2_OA2MSK>(); 	}
		auto OA2() 		noexcept { return base_t::template Actual<I2C_OAR2_OA2>(); 		}
		//
		//
	};
	//
	static std::uint64_t I2CCLK() noexcept
	{
		switch( RCCGeneral::CFGR3{}.I2CClockSource() )
		{
		case RCCGeneral::I2CClock::HSI: 	return ::System::HSIClock;
		case RCCGeneral::I2CClock::SYSCLK: 	return ::System::SystemClock;
		default:							return 0;
		};
	}
	//
	template <std::uint32_t C, std::size_t A = 0x10>
	struct TIMINGR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;
		//
		//
		auto PRESC() 	noexcept { return base_t::template Actual<I2C_TIMINGR_PRESC>(); 	}
		auto SCLDEL() 	noexcept { return base_t::template Actual<I2C_TIMINGR_SCLDEL>(); 	}
		auto SDADEL() 	noexcept { return base_t::template Actual<I2C_TIMINGR_SDADEL>(); 	}
		auto SCLH() 	noexcept { return base_t::template Actual<I2C_TIMINGR_SCLH>(); 		}
		auto SCLL() 	noexcept { return base_t::template Actual<I2C_TIMINGR_SCLL>(); 		}
		//
		// This field is used to prescale I2CCLK in order to generate the clock period t PRESC used for data
		// setup and hold counters (refer to I2C timings on page 623) and for SCL high and low level
		// counters (refer to I2C master initialization on page 638).
		bool ClockPeriod( float time ) noexcept
		{
			using namespace RCCGeneral;
			if ( auto clock = I2CCLK(); clock )
			{
				auto presc = time * (float)clock - 1.f;
				//
				if ( General::Between( presc, 0, 0xF ) )
				{
					PRESC() = presc;
					return true;
				}
			}
			else return false;
		}
		float ClockPeriod() noexcept
		{
			const auto 			clock = I2CCLK();
			const std::uint32_t presc = PRESC();
			//
			// t_PRESC = ( PRESC + 1 ) x t_I2CCLK
			return float( clock ) / float( presc + 1 );
		}
		//
		// This field is used to generate a delay t SCLDEL between SDA edge and SCL rising edge. In
		// master mode and in slave mode with NOSTRETCH = 0, the SCL line is stretched low during
		// t_SCLDEL
		bool DataSetupTime( float time ) noexcept
		{
			auto t_PRESC = ClockPeriod();
			//
			// t_SCLDEL = (SCLDEL+1) x t_PRESC
			auto value = time / float(t_PRESC) - float(1);
			if ( General::Between( value, 0, 0xF ) )
			{
				SCLDEL() = value;
				return true;
			}
			return false;
		}
		float DataSetupTime() noexcept
		{
			auto t_PRESC = ClockPeriod();
			//
			// t_SCLDEL = (SCLDEL+1) x t_PRESC
			std::uint32_t value = SCLDEL();
			return float(value + 1) * t_PRESC;
		}
		//
		// This field is used to generate the delay t_SDADEL between SCL falling edge and SDA edge. In
		// master mode and in slave mode with NOSTRETCH = 0, the SCL line is stretched low during
		// t_SDADEL.
		bool DataHoldTime(float time) noexcept
		{
			auto t_PRESC = ClockPeriod();
			//
			// t_SDADEL = SDADEL x t_PRESC
			std::uint32_t value = time / t_PRESC;
			if ( General::Between( value, 0, 0xF ) )
			{
				SDADEL() = value;
				return true;
			}
			return false;
		}
		float DataHoldTime() noexcept
		{
			auto t_PRESC = ClockPeriod();
			std::uint32_t value = SDADEL();
			// t_SDADEL = SDADEL x t_PRESC
			return value * t_PRESC;
		}
		//
		//
		bool HighTime(float time) noexcept
		{
			auto t_PRESC = ClockPeriod();
			// t_SCLH = ( SCLH + 1 ) x t_PRESC
			std::uint32_t value = time / t_PRESC - 1;
			if ( General::Between( value, 0, 0xFF ) )
			{
				SCLH() = value;
				return true;
			}
			return false;
		}
		float HighTime() noexcept
		{
			auto t_PRESC = ClockPeriod();
			// t_SCLH = ( SCLH + 1 ) x t_PRESC
			return ( float( (std::uint32_t)SCLH() ) + 1.f ) * t_PRESC;
		}
		//
		//
		bool LowTime(float time) noexcept
		{
			auto t_PRESC = ClockPeriod();
			// t_SCLH = ( SCLL + 1 ) x t_PRESC
			std::uint32_t value = time / t_PRESC - 1;
			if ( General::Between( value, 0, 0xFF ) )
			{
				SCLL() = value;
				return true;
			}
			return false;
		}
		float LowTime() noexcept
		{
			auto t_PRESC = ClockPeriod();
			// t_SCLL = ( SCLL + 1 ) x t_PRESC
			return ( float( (std::uint32_t)SCLL() ) + 1.f ) * t_PRESC;
		}
	};
	//
	enum class TimeoutType : std::uint32_t
	{
		LowSCL			= 0,
		LowSCLandSDA	= 1
	};
	//
	template <std::uint32_t C, std::size_t A = 0x14>
	struct TIMEOUTR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;
		//
		auto TEXTEN() 	noexcept { return base_t::template Actual<I2C_TIMEOUTR_TEXTEN>(); 	}
		auto TIMEOUTB() noexcept { return base_t::template Actual<I2C_TIMEOUTR_TIMEOUTB>(); }
		auto TIMOUTEN() noexcept { return base_t::template Actual<I2C_TIMEOUTR_TIMOUTEN>(); }
		auto TIDLE() 	noexcept { return base_t::template Actual<I2C_TIMEOUTR_TIDLE>(); 	}
		auto TIMEOUTA() noexcept { return base_t::template Actual<I2C_TIMEOUTR_TIMEOUTA>(); }
		//
		//
		using TIMINGR_t = TIMINGR<C>;
		//
		//
		bool TimeoutA(float time, TimeoutType type) noexcept
		{
			auto t_I2CCLK = I2CCLK();
			// t_TIMEOUT / t_I2CCLK / 2048 - 1 = TIMEOUTA
			std::uint32_t value = time / float(t_I2CCLK) / 2048.f - 1.f;
			if ( General::Between( value, 0u, 0xFFFu ) )
			{
				TIMEOUTA() = value;
				TIDLE() = (std::uint32_t)type;
				return true;
			}
			return false;
		}
		float TimeoutA() noexcept
		{
			auto t_I2CCLK = I2CCLK();
			float timeoutA = (std::uint32_t)TIMEOUTA();
			return (timeoutA + 1.f) * 2048.f * (float)t_I2CCLK;
		}
		//
		//
		bool TimeoutB(float time) noexcept
		{
			// t_TIMEOUT / t_I2CCLK / 2048 - 1 = TIMEOUTA
			auto t_I2CCLK = I2CCLK();
			std::uint32_t value = time / float(t_I2CCLK) / 2048.f - 1.f;
			if ( General::Between( value, 0u, 0xFFFu ) )
			{
				TIMEOUTB() = value;
				return true;
			}
			return false;
		}
		float TimeoutB() noexcept
		{
			auto t_I2CCLK = I2CCLK();
			float timeoutB = (std::uint32_t)TIMEOUTB();
			return (timeoutB + 1.f) * 2048.f * (float)t_I2CCLK;
		}
		//
		//
	};
	//
	template <std::uint32_t C, std::size_t A = 0x18>
	struct ISR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;
		//
		auto STOPF() 	noexcept { return base_t::template Actual<I2C_ISR_STOPF>(); 	}
		auto NACKF() 	noexcept { return base_t::template Actual<I2C_ISR_NACKF>(); 	}
		auto ADDCODE() 	noexcept { return base_t::template Actual<I2C_ISR_ADDCODE>(); 	}
		auto DIR() 		noexcept { return base_t::template Actual<I2C_ISR_DIR>(); 		}
		auto BUSY() 	noexcept { return base_t::template Actual<I2C_ISR_BUSY>(); 		}
		auto ALERT() 	noexcept { return base_t::template Actual<I2C_ISR_ALERT>(); 	}
		auto TIMEOUT() 	noexcept { return base_t::template Actual<I2C_ISR_TIMEOUT>(); 	}
		auto PECERR() 	noexcept { return base_t::template Actual<I2C_ISR_PECERR>(); 	}
		auto OVR() 		noexcept { return base_t::template Actual<I2C_ISR_OVR>(); 		}
		auto ARLO() 	noexcept { return base_t::template Actual<I2C_ISR_ARLO>(); 		}
		auto BERR() 	noexcept { return base_t::template Actual<I2C_ISR_BERR>(); 		}
		auto TCR()		noexcept { return base_t::template Actual<I2C_ISR_TCR>(); 		}
		auto TC() 		noexcept { return base_t::template Actual<I2C_ISR_TC>(); 		}
		auto ADDR() 	noexcept { return base_t::template Actual<I2C_ISR_ADDR>(); 		}
		auto RXNE() 	noexcept { return base_t::template Actual<I2C_ISR_RXNE>(); 		}
		auto TXIS() 	noexcept { return base_t::template Actual<I2C_ISR_TXIS>(); 		}
		auto TXE() 		noexcept { return base_t::template Actual<I2C_ISR_TXE>();		}
		//
	};
	//
	template <std::uint32_t C, std::size_t A = 0x1C>
	struct ICR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;
		//
		// Write 1 to clear registers, just do the write, cannot read this register
		void ALERTCF() 	noexcept { base_t::template Actual<I2C_ICR_ALERTCF>()	= 1; 	}
		void TIMOUTCF() noexcept { base_t::template Actual<I2C_ICR_TIMOUTCF>()	= 1; 	}
		void PECCF() 	noexcept { base_t::template Actual<I2C_ICR_PECCF>()		= 1; 	}
		void OVRCF() 	noexcept { base_t::template Actual<I2C_ICR_OVRCF>()		= 1; 	}
		void ARLOCF() 	noexcept { base_t::template Actual<I2C_ICR_ARLOCF>()	= 1; 	}
		void BERRCF() 	noexcept { base_t::template Actual<I2C_ICR_BERRCF>()	= 1; 	}
		void STOPCF() 	noexcept { base_t::template Actual<I2C_ICR_STOPCF>()	= 1; 	}
		void NACKCF() 	noexcept { base_t::template Actual<I2C_ICR_NACKCF>()	= 1; 	}
		void ADDRCF() 	noexcept { base_t::template Actual<I2C_ICR_ADDRCF>()	= 1; 	}
		//
		// Clear all
		void ClearAll() noexcept
		{
			base_t::operator=(0xFFFFu);
		}
	};
	//
	template <std::uint32_t C, std::size_t A = 0x20>
	struct PECR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;

		auto PEC() 	noexcept { return base_t::template Actual<I2C_PECR_PEC>(); }
	};
	//
	template <std::uint32_t C, std::size_t A = 0x24>
	struct RXDR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;

		auto RXDATA() 	noexcept { return base_t::template Actual<I2C_RXDR_RXDATA>(); }
	};
	//
	template <std::uint32_t C, std::size_t A = 0x28>
	struct TXDR : General::u32_reg<BaseAddress(C) + A>
	{
		using base_t = General::u32_reg<BaseAddress(C) + A>;
		using base_t::base_t;

		auto TXDATA() 	noexcept { return base_t::template Actual<I2C_TXDR_TXDATA>(); }
	};
}