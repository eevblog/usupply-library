#pragma once
#include "Meta.hpp"
#include "DataView.hpp"
#include "I2C.hpp"
#include "I2CBitbanging.hpp"
#include "USB_Types.hpp"
#include "PD/PDMessage.hpp"
#include <array>
#include <cstdint>
#include <optional>
/**
 * A general purpose UFP USB [P]ower [D]elivery library
 *  by : David Ledger ( EEVblog )
 * 
 * This library is for use with I2C [T]ype [C] [P]ort [C]ontrollers (TCPC)
 *  this libary was tested with Richtek RT1716, but should work with all common controllers
 *  the TCPC registers are standardised in:
 *  [T]ype [C] [P]ort [C]ontrollers
 * 
 *  Source : https://www.usb.org/sites/default/files/documents/usb-port_controller_specification_rev2.0_v1.0_0.pdf
 */
namespace USB::PD
{
	#pragma pack(push, 1)
    enum class Command : uint8_t
    {
        WakeI2C	                    =	0b00010001,
        DisableVbusDetect	        =	0b00100010,
        EnableVbusDetect	        =	0b00110011,
        DisableSinkVbus	            =	0b01000100,
        SinkVbus	                =	0b01010101,
        DisableSourceVbus	        =	0b01100110,
        SourceVbusDefaultVoltage	=	0b01110111,
        SourceVbusHighVoltage	    =	0b10001000,
        Look4Connection	            =	0b10011001,
        RxOneMore	                =	0b10101010,
        SendFRSwapSignal	        =	0b11001100,
        ResetTransmitBuffer	        =	0b11011101,
        ResetReceiveBuffer	        =	0b11101110,
        IdleI2C 	                =	0b11111111
    };
    /**
     * @brief A list of all messages that could be raised by TCPC.
     * 
     * These messages are sent into the operating system message queue.
     * This allows the use of continuations.
     */
    enum class TCPCMessage : uint32_t
    {
        ConnectResultRp,
        ConnectResultRd,
        PotentialConnectionFound,
        SinkVBus,
        VConnPresent,
        VBusPresent,
        VBusPresentDetect,
        SourceVBus,
        SourceHighVoltage,
        TCPCInitialisation,
        DebugAccessoryConnect
    };
    /**
     */
    enum class Registers : uint8_t
    {
        VENDOR_ID = 0x00,
        PRODUCT_ID = 0x02,
        DEVICE_ID = 0x04,
        USBTYPEC_REV = 0x06,
        USBPD_REV_VER = 0x08,
        PD_INTERFACE_REV = 0x0A,
        ALERT = 0x10,
        ALERT_L = 0x10,
        ALERT_H = 0x11,
        ALERT_MASK = 0x12,
        ALERT_MASK_L = 0x12,
        ALERT_MASK_H = 0x13,
        POWER_STATUS_MASK = 0x14,
        FAULT_STATUS_MASK = 0x15,
        CONFIG_STANDARD_OUTPUT = 0x18,
        TCPC_CONTROL = 0x19,
        ROLE_CONTROL = 0x1A,
        FAULT_CONTROL = 0x1B,
        POWER_CONTROL = 0x1C,
        CC_STATUS = 0x1D,
        POWER_STATUS = 0x1E,
        FAULT_STATUS = 0x1F,
        COMMAND = 0x23,
        DEVICE_CAPABILITIES = 0x24,
        DEVICE_CAPABILITIES_1L = 0x24,
        DEVICE_CAPABILITIES_1H = 0x25,
        DEVICE_CAPABILITIES_2L = 0x26,
        DEVICE_CAPABILITIES_2H = 0x27,
        STANDARD_INPUT_CAPABILITIES = 0x28,
        STANDARD_OUTPUT_CAPABILITIES = 0x29,
        MESSAGE_HEADER_INFO = 0x2E,
        RECEIVE_DETECT = 0x2F,
        RX_BYTE_COUNT = 0x30,
        RX_BUF_FRAME_TYPE = 0x31,
        RX_BUFFER = 0x32,
        RX_BUF_HEADER_BYTE_0 = 0x32,
        RX_BUF_HEADER_BYTE_1 = 0x33,
        RX_BUF_OBJ1_BYTE_0      = 0x34,
        RX_BUF_OBJ1_BYTE_1      = 0x35,
        RX_BUF_OBJ1_BYTE_2      = 0x36,
        RX_BUF_OBJ1_BYTE_3      = 0x37,
        RX_BUF_OBJ2_BYTE_0      = 0x38,
        RX_BUF_OBJ2_BYTE_1      = 0x39,
        RX_BUF_OBJ2_BYTE_2      = 0x3A,
        RX_BUF_OBJ2_BYTE_3      = 0x3B,
        RX_BUF_OBJ3_BYTE_0      = 0x3C,
        RX_BUF_OBJ3_BYTE_1      = 0x3D,
        RX_BUF_OBJ3_BYTE_2      = 0x3E,
        RX_BUF_OBJ3_BYTE_3      = 0x3F,
        RX_BUF_OBJ4_BYTE_0      = 0x40,
        RX_BUF_OBJ4_BYTE_1      = 0x41,
        RX_BUF_OBJ4_BYTE_2      = 0x42,
        RX_BUF_OBJ4_BYTE_3      = 0x43,
        RX_BUF_OBJ5_BYTE_0      = 0x44,
        RX_BUF_OBJ5_BYTE_1      = 0x45,
        RX_BUF_OBJ5_BYTE_2      = 0x46,
        RX_BUF_OBJ5_BYTE_3      = 0x47,
        RX_BUF_OBJ6_BYTE_0      = 0x48,
        RX_BUF_OBJ6_BYTE_1      = 0x49,
        RX_BUF_OBJ6_BYTE_2      = 0x4A,
        RX_BUF_OBJ6_BYTE_3      = 0x4B,
        RX_BUF_OBJ7_BYTE_0      = 0x4C,
        RX_BUF_OBJ7_BYTE_1      = 0x4D,
        RX_BUF_OBJ7_BYTE_2      = 0x4E,
        RX_BUF_OBJ7_BYTE_3      = 0x4F,
        TX_BUF_FRAME_TYPE       = 0x50,
        TX_BYTE_COUNT           = 0x51,
        TX_BUFFER               = 0x52,
        TX_BUF_HEADER_BYTE_0    = 0x52,
        TX_BUF_HEADER_BYTE_1    = 0x53,
        TX_BUF_OBJ1_BYTE_0      = 0x54,
        TX_BUF_OBJ1_BYTE_1      = 0x55,
        TX_BUF_OBJ1_BYTE_2      = 0x56,
        TX_BUF_OBJ1_BYTE_3      = 0x57,
        TX_BUF_OBJ2_BYTE_0      = 0x58,
        TX_BUF_OBJ2_BYTE_1      = 0x59,
        TX_BUF_OBJ2_BYTE_2      = 0x5A,
        TX_BUF_OBJ2_BYTE_3      = 0x5B,
        TX_BUF_OBJ3_BYTE_0      = 0x5C,
        TX_BUF_OBJ3_BYTE_1      = 0x5D,
        TX_BUF_OBJ3_BYTE_2      = 0x5E,
        TX_BUF_OBJ3_BYTE_3      = 0x5F,
        TX_BUF_OBJ4_BYTE_0      = 0x60,
        TX_BUF_OBJ4_BYTE_1      = 0x61,
        TX_BUF_OBJ4_BYTE_2      = 0x62,
        TX_BUF_OBJ4_BYTE_3      = 0x63,
        TX_BUF_OBJ5_BYTE_0      = 0x64,
        TX_BUF_OBJ5_BYTE_1      = 0x65,
        TX_BUF_OBJ5_BYTE_2      = 0x66,
        TX_BUF_OBJ5_BYTE_3      = 0x67,
        TX_BUF_OBJ6_BYTE_0      = 0x68,
        TX_BUF_OBJ6_BYTE_1      = 0x69,
        TX_BUF_OBJ6_BYTE_2      = 0x6A,
        TX_BUF_OBJ6_BYTE_3T     = 0x6B,
        X_BUF_OBJ7_BYTE_0       = 0x6C,
        TX_BUF_OBJ7_BYTE_1      = 0x6D,
        TX_BUF_OBJ7_BYTE_2      = 0x6E,
        TX_BUF_OBJ7_BYTE_3      = 0x6F,
        PD_MESSAGE_VERSION      = 0x9B
    };
    enum class RpValue : uint8_t
    {
        Rp_Default  = 0b00u,
        Rp_1_5A     = 0b01u,
        Rp_3_0A     = 0b10u,
        Rp_Reserved = 0b11u
    };
    /**
     * 
     */
    enum class CCResistor : unsigned
    {
        Ra      = 0b00u,
        Rp      = 0b01u,
        Rd      = 0b10u,
        Open    = 0b11u
    };
    enum class CCState : uint8_t
    {
// This is the default, it is shared with the two other modes.
        Open            = 0b000,
// If (ROLE_CONTROL.CC2=Rp) or (ConnectResult=0)
        SRC_Open        = 0b000,
        SRC_Ra          = 0b001,
        SRC_Rd          = 0b010,
// If (ROLE_CONTROL.CC2=Rd) or (ConnectResult=1)
        SNK_Open        = 0b100,
        SNK_Default     = 0b101,
        SNK_Power1_5    = 0b110,
        SNK_Power3_0    = 0b111
    };
    /**
     * 
     */
    enum class VCONNTarget : uint8_t
    {
        CC1 = 1,
        CC2 = 0
    };
    /**
     * 
     */
    enum class VConnPowerSupported : uint8_t
    {
        AtLeast1W = 0,
        DefinedInVConnPowerSupported = 1
    };

    enum class VENDOR_ID : uint16_t{};
    enum class PRODUCT_ID : uint16_t{};
    enum class DEVICE_ID : uint16_t{};
    //
    struct USBTYPEC_REV
    {
        uint8_t Reserved;
        uint8_t USBTYPEC_REV;
    };
    struct USBPD_REV_VER
    {
        uint8_t USBPD_REV;
        uint8_t USBPD_VER;
    };
    struct PD_INTERFACE_REV
    {
        uint8_t PDIF_REV;
        uint8_t PDIF_VER;
    };
    struct ALERT_L
    {
        uint8_t CC_STATUS : 1;
        uint8_t POWER_STATUS : 1;
        uint8_t RX_SOP_MSG_STATUS : 1;
        uint8_t RX_HARD_RESET : 1;
        uint8_t TX_FAIL : 1;
        uint8_t TX_DISCARD : 1;
        uint8_t TX_SUCCESS : 1;
        uint8_t ALARM_VBUS_VOLTAGE_H : 1;
    };
    struct ALERT_H
    {
        uint8_t ALARM_VBUS_VOLTAGE_L : 1;
        uint8_t FAULT : 1;
        uint8_t RXBUF_OVFLOW : 1;
        uint8_t VBUS_SINK_DISCNT : 1;
        uint8_t Reserved4_7 : 1;
    };
    /**
     * Helper to group the alert bits into a single struct
     */
    struct ALERT : ALERT_L, ALERT_H{};
    /**
     * 
     */
    struct ALERT_MASK_L
    {
        uint8_t M_CC_STATUS : 1;
        uint8_t M_POWER_STATUS : 1;
        uint8_t M_RX_SOP_MSG_STATUS : 1;
        uint8_t M_RX_HARD_RESET : 1;
        uint8_t M_TX_FAIL : 1;
        uint8_t M_TX_DISCARD : 1;
        uint8_t M_TX_SUCCESS : 1;
        uint8_t M_ALARM_VBUS_VOLTAGE_H : 1;
    };
    struct ALERT_MASK_H
    {
        uint8_t M_ALARM_VBUS_VOLTAGE_L : 1;
        uint8_t M_FAULT : 1;
        uint8_t M_RXBUF_OVFLOW : 1;
        uint8_t M_VBUS_SINK_DISCNT : 1;
        uint8_t Reserved4_7 : 4;
    };
    /**
     * Helper to group the alert mask bits into a single usable struct
     */
    struct ALERT_MASK : ALERT_MASK_L, ALERT_MASK_H
    {
    };
    /**
     * 
     */
    struct POWER_STATUS_MASK
    {
        uint8_t M_SINK_VBUS : 1;
        uint8_t M_VCONN_PRESENT : 1;
        uint8_t M_VBUS_PRESENT : 1;
        uint8_t M_VBUS_PRESENT_DETC : 1;
        uint8_t M_SRC_VBUS : 1;
        uint8_t M_SRC_HV : 1;
        uint8_t M_TCPC_INITIAL : 1;
        uint8_t Reserved : 1;
    };
    struct FAULT_STATUS_MASK
    {
        uint8_t M_I2C_ERROR : 1;
        uint8_t M_VCON_OC : 1;
        uint8_t M_VBUS_OV : 1;
        uint8_t M_VBUS_OC : 1;
        uint8_t M_FORCE_DISC_FAIL : 1;
        uint8_t M_AUTODISC_FAIL : 1;
        uint8_t M_FORCE_OFF_VBUS : 1;
        uint8_t M_VCON_OV : 1;
    };
    struct CONFIG_STANDARD_OUTPUT
    {
        uint8_t CONNECT_ORIENT      : 1;
        uint8_t CONNECT_PRESENT     : 1;
        uint8_t MUX_CTRL            : 2;
        uint8_t ACTIVE_CABLE_CONNECT : 1;
        uint8_t AUDIO_ACC_CONNECT   : 1;
        uint8_t DBG_ACC_CONNECT_O   : 1;
        uint8_t H_IMPEDENCE         : 1;
    };
    using COMMAND = Command;
    struct TCPC_CONTROL
    {
        uint8_t PLUG_ORIENT         : 1;
        uint8_t BIST_TEST_MODE      : 1;
        uint8_t I2C_CK_STRETCH      : 2;
        uint8_t Reserved4_7         : 4;
    };
    struct ROLE_CONTROL
    {
        CCResistor  CC1             : 2;
        CCResistor  CC2             : 2;
        RpValue     RP_VALUE        : 2;
        uint8_t     DRP             : 1;
        uint8_t     Reserved        : 1;
    };
    struct FAULT_CONTROL
    {
        uint8_t DIS_VCON_OC         : 1;
        uint8_t DIS_VBUS_OV         : 1;
        uint8_t DIS_VBUS_OC         : 1;
        uint8_t DIS_VBUS_DISC_FAULT_TIMER : 1;
        uint8_t DIS_FORCE_OFF_VBUS  : 1;
        uint8_t Reserved            : 2;
        uint8_t DIS_VCON_OV         : 1;
    };
    struct POWER_CONTROL
    {
        uint8_t EN_VCONN            : 1;
        uint8_t VCONN_POWER_SPT     : 1;
        uint8_t FORCE_DISC          : 1;
        uint8_t BLEED_DISC          : 1;
        uint8_t AUTO_DISC_DISCNCT   : 1;
        uint8_t DIS_VOL_ALARM       : 1;
        uint8_t VBUS_VOL_MONITOR    : 1;
        uint8_t Reserved            : 1;
    };
    //
    //
    enum class CCSink : uint8_t
    {
        Open        = 0b00,
        Default     = 0b01,
        Power1_5    = 0b10,
        Power3_0    = 0b11
    };
    enum class CCSource : uint8_t
    {
        Open        = 0b00,
        Ra          = 0b01,
        Rd          = 0b10
    };
    struct CC_STATUS
    {
        uint8_t CC1_STATE  : 2;
        uint8_t CC2_STATE  : 2;
        uint8_t DRP_RESULT : 1;
        uint8_t DRP_STATUS : 1;
        uint8_t Reserved   : 2;
    };
    struct POWER_STATUS
    {
        uint8_t SINK_VBUS : 1;
        uint8_t VCONN_PRESENT : 1;
        uint8_t VBUS_PRESENT : 1;
        uint8_t VBUS_PRESENT_DETC : 1;
        uint8_t SRC_VBUS : 1;
        uint8_t SRC_HV : 1;
        uint8_t TCPC_INITIAL : 1;
        uint8_t DBG_ACC_CONNECT : 1;
    };
    struct FAULT_STATUS
    {
        uint8_t I2C_ERROR       : 1;
        uint8_t VCON_OC         : 1;
        uint8_t VBUS_OV         : 1;
        uint8_t VBUS_OC         : 1;
        uint8_t FORCE_DISC_FAIL : 1;
        uint8_t AUTO_DISC_FAIL  : 1;
        uint8_t FORCE_OFF_VBUS  : 1;
        uint8_t VCON_OV         : 1;
    };
    struct DEVICE_CAPABILITIES_1L
    {
        uint8_t SOURCE_VBUS         : 1;
        uint8_t SOURCE_HV_VBUS      : 1;
        uint8_t CPB_SINK_VBUS       : 1;
        uint8_t SOURCE_VCONN        : 1;
        uint8_t ALL_SOP_SUPPORT     : 1;
        uint8_t ROLES_SUPPORT       : 3;
    };
    struct DEVICE_CAPABILITIES_1H
    {
        uint8_t SOURCE_RP_SUPPORT   : 2;
        uint8_t VBUS_MEASURE_ALARM  : 1;
        uint8_t CPB_FORCE_DISC      : 1;
        uint8_t CPB_BLEED_DISC      : 1;
        uint8_t CPB_VBUS_OV         : 1;
        uint8_t CPB_VBUS_OC         : 1;
        uint8_t Reserved_1H_1       : 1;
    };
    struct DEVICE_CAPABILITIES_2L
    {
        uint8_t VCONN_OCF           : 1;
        uint8_t VCONN_POWER         : 3;
        uint8_t VBUS_VOL_ALARM_LSB  : 2;
        uint8_t STOP_DISC_THD       : 1;
        uint8_t SINK_DISCONNECT_DET : 1;
    };
    struct DEVICE_CAPABILITIES_2H
    {
        uint8_t Reserved_2H_1;
    };
    /**
     * Helper struct to group together all the various capibilites
     */
    struct DEVICE_CAPABILITIES : DEVICE_CAPABILITIES_1L, DEVICE_CAPABILITIES_1H, DEVICE_CAPABILITIES_2L, DEVICE_CAPABILITIES_2H {};
    /**
     * 
     */
    struct STANDARD_INPUT_CAPABILITIES
    {
        uint8_t FORCE_OFF_VBUS_IN : 1;
        uint8_t VBUS_EXT_OCF : 1;
        uint8_t VBUS_EXT_OVF : 1;
        uint8_t Reserved : 5;
    };
    struct STANDARD_OUTPUT_CAPABILITIES
    {
        uint8_t CPB_CONNECT_ORIENT : 1;
        uint8_t CPB_CONNECT_PRESENT : 1;
        uint8_t CPB_MUX_CFG_CTRL : 1;
        uint8_t CPB_ACTIVE_CABLE_IND : 1;
        uint8_t CPB_AUDIO_ADT_ACC_IND : 1;
        uint8_t CPB_VBUS_PRESENT_MNT : 1;
        uint8_t CPB_DBG_ACC_IND : 1;
        uint8_t Reserved : 1;
    };
    struct MESSAGE_HEADER_INFO
    {
        PortPowerRole           POWER_ROLE      : 1;
        SpecificationRevision   USBPD_SPECREV   : 2;
        PortDataRole            DATA_ROLE       : 1;
        CablePlug               CABLE_PLUG      : 1;
        uint8_t                 Reserved        : 3;
        //
        MESSAGE_HEADER_INFO( PortPowerRole power_role, SpecificationRevision spec_rev, PortDataRole data_role, CablePlug cable_plug = CablePlug::OriginatedFromDFPorUFP) : 
            POWER_ROLE      { power_role },
            USBPD_SPECREV   { spec_rev },
            DATA_ROLE       { data_role },
            CABLE_PLUG      { cable_plug },
            Reserved        {0}
        {}
    };
    struct RECEIVE_DETECT
    {
        uint8_t EN_SOP : 1;
        uint8_t EN_SOP1 : 1;
        uint8_t EN_SOP2 : 1;
        uint8_t EN_SOP1DB : 1;
        uint8_t EN_SOP2DB : 1;
        uint8_t EN_HARD_RST : 1;
        uint8_t EN_CABLE_RST : 1;
        uint8_t Reserved : 1;
    };
    using RX_BYTE_COUNT = uint8_t;
    //
    enum class RxFrameType : uint8_t
    {
        SOP                  = 0b000,
        SOP_Prime            = 0b001,
        SOP_DoublePrime      = 0b010,
        SOP_DebugPrime       = 0b011,
        SOP_DebugDoublePrime = 0b100,
        CableReset           = 0b110
    };
    //
    struct RX_BUF_FRAME_TYPE
    {
        RxFrameType RX_FRAME_TYPE : 3;
        uint8_t Reserved : 5;
    };
    struct RX_BUF_HEADER_BYTE_0
    {
        uint8_t RX_HEAD_0;
    };
    struct RX_BUF_HEADER_BYTE_1
    {
        uint8_t RX_HEAD_1;
    };
    struct RX_BUF_OBJ1_BYTE_0
    {
        uint8_t RX_OBJ1_0;
    };
    struct RX_BUF_OBJ1_BYTE_1
    {
        uint8_t RX_OBJ1_1;
    };
    struct RX_BUF_OBJ1_BYTE_2
    {
        uint8_t RX_OBJ1_2;
    };
    struct RX_BUF_OBJ1_BYTE_3
    {
        uint8_t RX_OBJ1_3;
    };
    struct RX_BUF_OBJ2_BYTE_0
    {
        uint8_t RX_OBJ2_0;
    };
    struct RX_BUF_OBJ2_BYTE_1
    {
        uint8_t RX_OBJ2_1;
    };
    struct RX_BUF_OBJ2_BYTE_2
    {
        uint8_t RX_OBJ2_2;
    };
    struct RX_BUF_OBJ2_BYTE_3
    {
        uint8_t RX_OBJ2_3;
    };
    struct RX_BUF_OBJ3_BYTE_0
    {
        uint8_t RX_OBJ3_0;
    };
    struct RX_BUF_OBJ3_BYTE_1
    {
        uint8_t RX_OBJ3_1;
    };
    struct RX_BUF_OBJ3_BYTE_2
    {
        uint8_t RX_OBJ3_2;
    };
    struct RX_BUF_OBJ3_BYTE_3
    {
        uint8_t RX_OBJ3_3;
    };
    struct RX_BUF_OBJ4_BYTE_0
    {
        uint8_t RX_OBJ4_0;
    };
    struct RX_BUF_OBJ4_BYTE_1
    {
        uint8_t RX_OBJ4_1;
    };
    struct RX_BUF_OBJ4_BYTE_2
    {
        uint8_t RX_OBJ4_2;
    };
    struct RX_BUF_OBJ4_BYTE_3
    {
        uint8_t RX_OBJ4_3;
    };
    struct RX_BUF_OBJ5_BYTE_0
    {
        uint8_t RX_OBJ5_0;
    };
    struct RX_BUF_OBJ5_BYTE_1
    {
        uint8_t RX_OBJ5_1;
    };
    struct RX_BUF_OBJ5_BYTE_2
    {
        uint8_t RX_OBJ5_2;
    };
    struct RX_BUF_OBJ5_BYTE_3
    {
        uint8_t RX_OBJ5_3;
    };
    struct RX_BUF_OBJ6_BYTE_0
    {
        uint8_t RX_OBJ6_0;
    };
    struct RX_BUF_OBJ6_BYTE_1
    {
        uint8_t RX_OBJ6_1;
    };
    struct RX_BUF_OBJ6_BYTE_2
    {
        uint8_t RX_OBJ6_2;
    };
    struct RX_BUF_OBJ6_BYTE_3
    {
        uint8_t RX_OBJ6_3;
    };
    struct RX_BUF_OBJ7_BYTE_0
    {
        uint8_t RX_OBJ7_0;
    };
    struct RX_BUF_OBJ7_BYTE_1
    {
        uint8_t RX_OBJ7_1;
    };
    struct RX_BUF_OBJ7_BYTE_2
    {
        uint8_t RX_OBJ7_2;
    };
    struct RX_BUF_OBJ7_BYTE_3
    {
        uint8_t RX_OBJ7_3;
    };
    using TX_BUF_FRAME_TYPE = TxFrame;
    struct TX_BYTE_COUNT
    {
        uint8_t TX_BYTE_COUNT;
    };
    struct TX_BUF_HEADER_BYTE_0
    {
        uint8_t TX_HEAD_0;
    };
    struct TX_BUF_HEADER_BYTE_1
    {
        uint8_t TX_HEAD_1;
    };
    struct TX_BUF_OBJ1_BYTE_0
    {
        uint8_t TX_OBJ1_0;
    };
    struct TX_BUF_OBJ1_BYTE_1
    {
        uint8_t TX_OBJ1_1;
    };
    struct TX_BUF_OBJ1_BYTE_2
    {
        uint8_t TX_OBJ1_2;
    };
    struct TX_BUF_OBJ1_BYTE_3
    {
        uint8_t TX_OBJ1_3;
    };
    struct TX_BUF_OBJ2_BYTE_0
    {
        uint8_t TX_OBJ2_0;
    };
    struct TX_BUF_OBJ2_BYTE_1
    {
        uint8_t TX_OBJ2_1;
    };
    struct TX_BUF_OBJ2_BYTE_2
    {
        uint8_t TX_OBJ2_2;
    };
    struct TX_BUF_OBJ2_BYTE_3
    {
        uint8_t TX_OBJ2_3;
    };
    struct TX_BUF_OBJ3_BYTE_0
    {
        uint8_t TX_OBJ3_0;
    };
    struct TX_BUF_OBJ3_BYTE_1
    {
        uint8_t TX_OBJ3_1;
    };
    struct TX_BUF_OBJ3_BYTE_2
    {
        uint8_t TX_OBJ3_2;
    };
    struct TX_BUF_OBJ3_BYTE_3
    {
        uint8_t TX_OBJ3_3;
    };
    struct TX_BUF_OBJ4_BYTE_0
    {
        uint8_t TX_OBJ4_0;
    };
    struct TX_BUF_OBJ4_BYTE_1
    {
        uint8_t TX_OBJ4_1;
    };
    struct TX_BUF_OBJ4_BYTE_2
    {
        uint8_t TX_OBJ4_2;
    };
    struct TX_BUF_OBJ4_BYTE_3
    {
        uint8_t TX_OBJ4_3;
    };
    struct TX_BUF_OBJ5_BYTE_0
    {
        uint8_t TX_OBJ5_0;
    };
    struct TX_BUF_OBJ5_BYTE_1
    {
        uint8_t TX_OBJ5_1;
    };
    struct TX_BUF_OBJ5_BYTE_2
    {
        uint8_t TX_OBJ5_2;
    };
    struct TX_BUF_OBJ5_BYTE_3
    {
        uint8_t TX_OBJ5_3;
    };
    struct TX_BUF_OBJ6_BYTE_0
    {
        uint8_t TX_OBJ6_0;
    };
    struct TX_BUF_OBJ6_BYTE_1
    {
        uint8_t TX_OBJ6_1;
    };
    struct TX_BUF_OBJ6_BYTE_2
    {
        uint8_t TX_OBJ6_2;
    };
    struct TX_BUF_OBJ6_BYTE_3T
    {
        uint8_t TX_OBJ6_3;
    };
    struct X_BUF_OBJ7_BYTE_0
    {
        uint8_t TX_OBJ7_0;
    };
    struct TX_BUF_OBJ7_BYTE_1
    {
        uint8_t TX_OBJ7_1;
    };
    struct TX_BUF_OBJ7_BYTE_2
    {
        uint8_t TX_OBJ7_2;
    };
    struct TX_BUF_OBJ7_BYTE_3
    {
        uint8_t TX_OBJ7_3;
    };
    struct PD_MESSAGE_VERSION
    {
        uint8_t Reserved : 4;
        uint8_t ENEXTMSG : 1;
    };
    /**
     * 
     */
    template <typename I2C, uint8_t Address>
    class TCPM : public I2C
    {
    public:
        using write_dataview    = typename I2C::write_dataview;
        using read_dataview     = typename I2C::read_dataview;
    private:
        /**
         * The TCPC I2C address
         */
        static inline constexpr IO::Address<7u> const SafeAddress = Address;
        template <typename T>
        using opt = std::optional<T>;
        /**
         * Generic register representation
         */
        template <typename T, bool WriteValue>
        struct Register 
        {
            struct
            {
                uint8_t m_Register;
                T       m_Value;
            } m_Data;
            USB::CriticalSection section{};

            ///using output_t = std::conditional_t<WriteValue, bool, opt<T>>;
            /**
             * @brief Runs the command for a register read or write
             * 
             * @param address The address to read or write.
             * @return T const* A pointer to the result or nullptr if the write failed.
             * if the transaction is only a read then the pointer still results in a pointer to value.
             * 
             * Pointers are convertable to bool.
             */
            decltype(auto) Run( IO::Address<7u> address ) const noexcept
            {
                if constexpr ( WriteValue )
                {
                    return I2C::Write( address, RBytes( m_Data ) );
                }
                else
                {
                    I2C::Mixed( address, RBytes( m_Data.m_Register ), WBytes( m_Data.m_Value ) );
                    return m_Data.m_Value;
                }
            }
        };
        struct ReceiveBufferHelper
        {
            uint8_t                 m_Register;
            struct Header
            {
                RX_BYTE_COUNT       m_Count;
                RX_BUF_FRAME_TYPE   m_FrameType;
            } mutable m_Header;
            read_dataview mutable   m_ReceiveData;
            USB::CriticalSection    m_Section{};
            /**
             * @brief Runs a receive command on a register.
             * 
             * @param address The address to receive data.
             * @return T const* A pointer to the resulting receive header, nullptr if the transaction failed.
             */
            bool Run( IO::Address<7u> address ) const noexcept
            {
                return ( sizeof(Header) + m_ReceiveData.Size() ) == I2C::Mixed( address, RBytes( m_Register ), WBytes( m_Header ), m_ReceiveData );
            }
        };
        struct TransmitBufferHelper
        {
            Registers               m_Register;
            write_dataview          m_TransmitData;
            USB::CriticalSection    m_Section{};
            /**
             * @brief Transmits data to the TCPC
             * 
             * @param address The address to write to.
             * @return true when the tranaction( the write ) successfully completed.
             * @return false when the transaction failed.
             */
            bool Run( IO::Address<7u> address ) const noexcept
            {
                auto receive_view = RBytes( m_Register );
                return 
                {
                    ( sizeof(m_Register) + m_TransmitData.Size() ) ==
                    I2C::Write( address, receive_view, m_TransmitData )
                };
            }
        };
    public:
        /**
         * @brief 
         * 
         * @tparam T 
         * @param reg 
         * @param value 
         * @return true 
         * @return false 
         */
        template <typename R, typename T>
        static decltype(auto) WriteRegister(R reg, T const & value) noexcept
        {
            return Register<T, true>{ General::UnderlyingValue( reg ), value, {} }.Run( SafeAddress );
        }
        /**
         * @brief 
         * 
         * @tparam T 
         * @param reg 
         * @param result 
         * @return true 
         * @return false 
         */
        template <typename R, typename T>
        static decltype(auto) ReceiveRegister( R reg ) noexcept
        {
            return Register<T, false>{ { General::UnderlyingValue( reg ), {} }, {} }.Run( SafeAddress );
        }
        /**
         * These macros save alot of code duplication it could be done with
         *  a template but I didn't because reasons...
         */
        #define READ_REG_CAST(x, cast_type)                         \
        []() -> cast_type {                                         \
            auto r = ReceiveRegister<Registers, x>(Registers::x);   \
            return (cast_type const &)r;                            \
        }()
        //
        //
        #define READ_REG(x)                                             \
        [] () -> x{                                                     \
            return ReceiveRegister<Registers, x>(Registers::x);         \
        }()
        #define WRITE_REG(r, x)                                         \
        [](r arg1) -> bool {                                            \
            return WriteRegister(Registers::r, arg1);        \
        }(x)
        //
        //
        #define GETTER(name) (name)
        #define SETTER(name) (name)
        #define GENERATE_GETTER(result, name) static result GETTER(name)() noexcept { return READ_REG(result); }
        #define GENERATE_SETTER(result, name) static bool SETTER(name)(result input) noexcept { return WRITE_REG(result, input); }
        #define GENERATE_GETTER_SETTER_PAIR(result, name) GENERATE_GETTER(result, name) GENERATE_SETTER(result, name)
        /**
         * When using this class it is important to know:
         *  [S]tart [O]f [P]acket (SOP)
         *  __________    _______________            ________________    _________
         *  |        |  __|             |            |              |__  |        |
         *  |DFP(SOP)| |__| Plug 1(SOP')|============| Plug 2(SOP'')|__| |UFP(SOP)|
         *  |________|    |_____________|            |______________|    |________|
         * 
         *  SOP framing of a packet sends data to UFP, DFP or a Device.
         *  SOP' framing directs packets to the plug at the first end of the cable.
         *  SOP'' framing directs packets to the plug at the second end of the cable.
         * 
         *  Source : https://blog.teledynelecroy.com/2016/05/usb-type-c-and-power-delivery-messaging.html
         */
        //
        // Read only registers
        // These macros generate getters for the following registers.
        GENERATE_GETTER( PRODUCT_ID,        ProductID )
        GENERATE_GETTER( VENDOR_ID,         VendorID )
        GENERATE_GETTER( DEVICE_ID,         DeviceID )
        GENERATE_GETTER( USBTYPEC_REV,      TypeCRevision )
        GENERATE_GETTER( USBPD_REV_VER,     USBPDRevision )
        GENERATE_GETTER( RX_BYTE_COUNT,     ReceiveByteCount )
        GENERATE_GETTER( RX_BUF_FRAME_TYPE, ReceiveFrameType )
        //
        // Read and Write registers
        // These macros generate getter and setters for all the registers in the device
        GENERATE_GETTER_SETTER_PAIR( ALERT_MASK,                    AlertMask )
        GENERATE_GETTER_SETTER_PAIR( FAULT_STATUS_MASK,             FaultStatusMask )
        GENERATE_GETTER_SETTER_PAIR( POWER_STATUS_MASK,             PowerStatusMask )
        GENERATE_GETTER_SETTER_PAIR( ALERT,                         Alert )
        GENERATE_GETTER_SETTER_PAIR( FAULT_STATUS,                  FaultStatus )
        GENERATE_GETTER_SETTER_PAIR( POWER_STATUS,                  PowerStatus )
        GENERATE_GETTER_SETTER_PAIR( CC_STATUS,                     CCStatus )
        GENERATE_GETTER_SETTER_PAIR( DEVICE_CAPABILITIES,           DeviceCapibilies )
        GENERATE_GETTER_SETTER_PAIR( STANDARD_INPUT_CAPABILITIES,   StandardInputCapibilities )
        GENERATE_GETTER_SETTER_PAIR( STANDARD_OUTPUT_CAPABILITIES,  StandardOutputCapibilities )
        GENERATE_GETTER_SETTER_PAIR( TCPC_CONTROL,                  TCPCControl )
        GENERATE_GETTER_SETTER_PAIR( ROLE_CONTROL,                  RoleControl )
        GENERATE_GETTER_SETTER_PAIR( FAULT_CONTROL,                 FaultControl )
        GENERATE_GETTER_SETTER_PAIR( POWER_CONTROL,                 PowerControl )
        GENERATE_GETTER_SETTER_PAIR( MESSAGE_HEADER_INFO,           MessageHeaderInformation )
        GENERATE_GETTER_SETTER_PAIR( RECEIVE_DETECT,                ReceiveDetect )
        GENERATE_GETTER_SETTER_PAIR( TX_BUF_FRAME_TYPE,             WriteFrameType )
        GENERATE_GETTER_SETTER_PAIR( TX_BYTE_COUNT,                 WriteByteCount )
        //
        // Write only registers
        GENERATE_SETTER( COMMAND, Command );
        /**
         * @brief 
         * 
         * @return CCState 
         */
        static CCState CC1State() noexcept
        {
            auto cc_status_fields = GETTER(CCStatus)();
            auto role_control = GETTER(RoleControl)();
            //

            
            if ( role_control.CC1 == CCResistor::Rp or not cc_status_fields.DRP_RESULT )
            {
                /**
                 * The CC state is set as a source
                 */
                uint8_t state = 0b000 | (cc_status_fields.CC1_STATE & 0b11);
                /**
                 * Bit compatible with CCState now.
                 */
                return (CCState)state;
            }
            else if ( role_control.CC1 == CCResistor::Rd or cc_status_fields.DRP_RESULT )
            {
                /**
                 * The CC state is set as a sink
                 */
                uint8_t state = 0b100 |  (cc_status_fields.CC1_STATE & 0b11);
                /**
                 * Bit compatible with CCState now.
                 */
                return (CCState)state;
            }
            else return CCState::Open;
        }
        /**
         * @brief 
         * 
         * @return CCState 
         */
        static CCState CC2State() noexcept
        {
            auto cc_status_fields = CCStatus();
            //
            if ((RoleControl().CC2 == CCResistor::Rp) || (cc_status_fields.DRP_RESULT == 0))
            {
                /**
                 * The CC state is set as a source
                 */
                uint8_t state = 0b000 | (cc_status_fields.CC2_STATE & 0b11);
                /**
                 * Bit compatible with CCState now.
                 */
                return (CCState)state;
            }
            else if ((RoleControl().CC2 == CCResistor::Rd) || (cc_status_fields.DRP_RESULT == 1))
            {
                /**
                 * The CC state is set as a sink
                 */
                uint8_t state = 0b100 |  (cc_status_fields.CC2_STATE & 0b11);
                /**
                 * Bit compatible with CCState now.
                 */
                return (CCState)state;
            }
            else return CCState::Open;
        }
        /**
         * @brief Enable receive on the TCPM
         * 
         * @return true Successfully enabled receive.
         * @return false Coudln't enable receive.
         */
        static bool ReceiveEnable() noexcept
        {
            RECEIVE_DETECT value{};
            value.EN_SOP = 1;
            return ReceiveDetect( value );
        }
        /**
         * @brief Triggers a hard reset.
         * 
         * @return true Successful transmitted reset packet.
         * @return false 
         */
        static bool HardReset() noexcept
        {
            RECEIVE_DETECT value{};
            value.EN_HARD_RST = 1;
            return ReceiveDetect( value );
        }
        /**
         * @brief Triggers a reset on the cable.
         * 
         * @return true Successfully transmitted reset packet.
         * @return false 
         */
        static bool CableReset() noexcept
        {
            RECEIVE_DETECT value{};
            value.EN_CABLE_RST = 1;
            return ReceiveDetect( value );
        }
        /**
         * @brief Transmits input view to the TCPM. 
         * 
         * @param input The dataview.
         * @return true When the transmit was completed.
         * @return false When transmit couldn't complete.
         */
        static bool TransmitBuffer( write_dataview input ) noexcept
        {
            return TransmitBufferHelper{ Registers::TX_BUFFER, input, {} }.Run( SafeAddress );
        }
        /**
         * @brief Get the RX buffer bytes
         *  The input parameter "buffer" defines the number of bytes to read.
         * 
         * The output parameter provides the bulk storage for the RX_BUF_BYTE_x's
         * 
         * @param output The view to receive into.
         * @return std::optional< decltype( std::declval<ReceiveBufferHelper>().m_Header ) > .has_value() when receive was completed successfully.
         */
        using ReceiveBufferOutput_t =std::optional< decltype( std::declval<ReceiveBufferHelper>().m_Header ) >;
        static auto ReceiveBuffer( read_dataview output ) noexcept -> ReceiveBufferOutput_t
        {
            ReceiveBufferHelper helper
            {
                Registers::RX_BUFFER,
                { {}, {} },
                output,
                {}
            };
            /**
             */
            return 
            {
                ( helper.Run( SafeAddress ) ) ? 
                    helper.m_Header : 
                    std::nullopt
            };
        }
    #undef WRITE_REG
    #undef READ_REG_CAST
    #undef READ_REG
    };
}