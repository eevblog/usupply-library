#pragma once
#include "Macros.hpp"
#include "AlternateFunction.hpp"
#include "PinCommon.hpp"
#include "Power.hpp"
#include "VirtualPin.hpp"

#include "stm32f0xx_rcc.h" //TODO: This needs to be removed and replaced.
#include <functional>
#include <type_traits>

namespace IO
{
	template <Port port>
	struct PinPowerKernal
	{
		//Port A, starts at bit 17
		static constexpr auto offset = (std::uint32_t)port + 17;
		static constexpr auto mask   = 1u << offset;

		static void Construct() noexcept
		{
			RCC->AHBENR |= mask;
		}
		static void Destruct() noexcept
		{
			RCC->AHBENR &= ~mask;
		}
	};

	template <Port port, std::uint32_t pin>
	class Pin : General::ModulePower<PinPowerKernal<port> >
	{
	public:
		using AltFunctions = AlternateFunction<port, pin>;
		static constexpr uint32_t PinIndex = pin;
		static constexpr uint32_t PortIndex = General::UnderlyingValue(port);
	private:
		using port_ptr = GPIO_TypeDef *;
		ALWAYS_INLINE static constexpr auto GetPort() noexcept
		{
			return (port_ptr)( GPIOA_BASE + ( GPIOB_BASE - GPIOA_BASE ) * (std::uint32_t)port );
		}

		static constexpr std::uint32_t PairShift = pin * 2u;
		static constexpr std::uint32_t PairMask  = ( 3u << PairShift );
		static constexpr std::uint16_t BitShift  = pin;
		static constexpr std::uint16_t BitMask   = ( 1u << ( BitShift ) );
		/**
		 * 
		 */
		ALWAYS_INLINE static void Set( Mode input ) noexcept
		{
			auto temp = GetPort()->MODER;
			temp &= ~PairMask;
			temp |= (std::uint16_t)input << PairShift;

			GetPort()->MODER = temp;
		}
		ALWAYS_INLINE static void Set( Type input ) noexcept
		{
			auto temp = GetPort()->OTYPER;
			temp &= ~BitMask;
			temp |= (std::uint16_t)input << BitShift;

			GetPort()->OTYPER = temp;
		}
		ALWAYS_INLINE static void Set( Speed input ) noexcept
		{
			auto temp = GetPort()->OSPEEDR;
			temp &= ~PairMask;
			temp |= (std::uint16_t)input << PairShift;

			GetPort()->OSPEEDR = temp;
		}
		ALWAYS_INLINE static void Set( Resistors input ) noexcept
		{
			auto temp = GetPort()->PUPDR;
			temp &= ~PairMask;
			temp |= (std::uint16_t)input << PairShift;
			GetPort()->PUPDR = temp;
		}
		ALWAYS_INLINE static void Set( bool input ) noexcept
		{
			if (input)	GetPort()->BSRR = BitMask;
			else		GetPort()->BRR = BitMask;
		}
		ALWAYS_INLINE static void Set( State input ) noexcept
		{
			Set( input == State::High );
		}
		ALWAYS_INLINE static void Set( typename AltFunctions::Type input ) noexcept
		{
			constexpr auto shift = ( pin % 8u ) * 4u;
			constexpr auto afr_i = ( std::uint32_t )( pin >= 8u );
			constexpr auto mask  = ( 0xFu << shift );

			const auto value        = (std::uint32_t)input;
			const auto temp         = value << shift;
			const auto copy         = GetPort()->AFR[ afr_i ] & ~mask;
			GetPort()->AFR[ afr_i ] = ( copy | temp );
		}
		ALWAYS_INLINE static void Set( IO::InterruptEdge input ) noexcept
		{
			System::InterruptGeneral::EXTIChannel<pin>{}.Set( input );
		}
		ALWAYS_INLINE static void Set( IO::Interrupt input ) noexcept
		{
			System::InterruptGeneral::EXTIChannel<pin>{}.Set( input );
		}
		ALWAYS_INLINE static void Set( IO::Event input ) noexcept
		{
			System::InterruptGeneral::EXTIChannel<pin>{}.Set( input );
		}
		/**
		 * 
		 */
		template <typename d, typename T>
		struct Get
		{};
		template <typename d>
		struct Get<d, IO::InterruptEdge>
		{
			ALWAYS_INLINE operator InterruptEdge() const noexcept
			{
				return System::InterruptGeneral::EXTIChannel<pin>{}.template Get<IO::InterruptEdge>();
			}
		};
		template <typename d>
		struct Get<d, IO::Interrupt>
		{
			ALWAYS_INLINE operator Interrupt() const noexcept
			{
				return System::InterruptGeneral::EXTIChannel<pin>{}.template Get<IO::Interrupt>();
			}
		};
		template <typename d>
		struct Get<d, IO::Event>
		{
			ALWAYS_INLINE operator Event() const noexcept
			{
				return System::InterruptGeneral::EXTIChannel<pin>{}.template Get<IO::Event>();
			}
		};
		template <typename d>
		struct Get<d, Mode>
		{
			ALWAYS_INLINE operator Mode() const noexcept
			{
				auto output = GetPort()->MODER;
				output &= PairMask;
				return output >> PairShift;
			}
		};
		template <typename d>
		struct Get<d, Type>
		{
			ALWAYS_INLINE operator Type() const noexcept
			{
				auto output = GetPort()->OTYPER;
				output &= BitMask;
				return output >> BitShift;
			}
		};
		template <typename d>
		struct Get<d, Speed>
		{
			ALWAYS_INLINE operator Speed() const noexcept
			{
				auto output = GetPort()->OSPEEDR;
				output &= PairMask;
				return output >> PairShift;
			}
		};
		template <typename d>
		struct Get<d, Resistors>
		{
			ALWAYS_INLINE operator Resistors() const noexcept
			{
				auto output = GetPort()->PUPDR;
				output &= PairMask;
				return output >> PairShift;
			}
		};
		template <typename d>
		struct Get<d, State>
		{
			ALWAYS_INLINE operator State() const noexcept
			{
				return ( State )( ( GetPort()->IDR & BitMask ) >> BitShift );
			}
			ALWAYS_INLINE operator bool() const noexcept
			{
				return ( ( GetPort()->IDR & BitMask ) >> BitShift );
			}
		};
		template <typename d>
		struct Get<d, AltFunctions>
		{
			ALWAYS_INLINE operator typename AltFunctions::Type() const noexcept
			{
				constexpr auto shift = ( pin % 8u ) * 4u;
				constexpr auto afr_i = ( std::uint32_t )( pin >= 8u );
				constexpr auto mask  = ( 0xFu << shift );
				const auto     copy  = GetPort()->AFR[ afr_i ] & mask;
				return ( copy >> shift );
			}
		};
		/**
		 * 
		 */
		template <typename T>
		ALWAYS_INLINE void Setup( T input ) noexcept
		{
			Set( input );
		}
		/**
		 * 
		 */
	public:
		/**
		 * The corresponding interrupt flags
		 */
		ALWAYS_INLINE static bool PendingInterruptFlag() noexcept
		{
			return System::InterruptGeneral::EXTIChannel<pin>{}.PendingFlag();
		}
		ALWAYS_INLINE static void ClearInterruptFlag() noexcept
		{
			System::InterruptGeneral::EXTIChannel<pin>{}.ClearFlag();
		}
		/**
		 * Software interrupt event register
		 */
		ALWAYS_INLINE static void TriggerInterrupt() noexcept
		{
			System::InterruptGeneral::EXTIChannel<pin>{}.Trigger();
		}
		/**
		 * 
		 */
		template <typename ... Args>
		ALWAYS_INLINE static void Configure( Args ... args ) noexcept
		{
			( Pin::Set( args ), ... );
		}
		/**
		 * 
		 */
		template <typename T>
		ALWAYS_INLINE Pin & operator=( T const input ) noexcept
		{
			Set( input );
			return *this;
		}
		template <typename T>
		ALWAYS_INLINE operator T() const noexcept
		{
			return Get<void, T>();
		}
		ALWAYS_INLINE operator bool() const noexcept
		{
			return Get<void, State>();
		}
		ALWAYS_INLINE Pin & operator=( bool const input ) noexcept
		{
			Set( (State)input );
			return *this;
		}
		ALWAYS_INLINE static void Toggle() noexcept
		{
			Set((State)!(bool)(State)Get<void, State>());
		}
		ALWAYS_INLINE static Get<void, State> Read() noexcept
		{
			return {};
		}
		ALWAYS_INLINE static void Write( bool input ) noexcept
		{
			Set( (State)input );
		}
		ALWAYS_INLINE static void Write( State input ) noexcept
		{
			Set( input );
		}
		/**
		 * 
		 */
		template <typename ... Args>
		ALWAYS_INLINE Pin( Args ... args ) noexcept
		{
			Configure( args ... );
		}
	};
}